package com.restfiuktarsasaga.ecarrentapi.service;

import com.restfiuktarsasaga.ecarrentapi.dao.CarDAO;
import com.restfiuktarsasaga.ecarrentapi.dao.RentDAO;
import com.restfiuktarsasaga.ecarrentapi.dao.UserDAO;
import com.restfiuktarsasaga.ecarrentapi.dto.dateinterval.DateIntervalDTO;
import com.restfiuktarsasaga.ecarrentapi.dto.rent.CreateRentDTO;
import com.restfiuktarsasaga.ecarrentapi.dto.rent.RentDTO;
import com.restfiuktarsasaga.ecarrentapi.dto.user.UserDTO;
import com.restfiuktarsasaga.ecarrentapi.entity.Car;
import com.restfiuktarsasaga.ecarrentapi.entity.Rent;
import com.restfiuktarsasaga.ecarrentapi.entity.User;
import com.restfiuktarsasaga.ecarrentapi.exception.NotFoundCarException;
import com.restfiuktarsasaga.ecarrentapi.exception.NotFoundRentException;
import com.restfiuktarsasaga.ecarrentapi.exception.NotFoundUserException;
import com.restfiuktarsasaga.ecarrentapi.service.interfaces.RentService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.RandomStringUtils;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Data
@RequiredArgsConstructor
public class RentServiceImpl implements RentService {

    private final CarDAO carDAO;

    private final UserDAO userDAO;

    private final RentDAO rentDAO;

    private final ModelMapper modelMapper;

    private static Type targetListType = new TypeToken<List<RentDTO>>() {}.getType();

    @Override
    public RentDTO createRent(CreateRentDTO createRentDTO, long carId, long userId) {
        User user = userDAO.findById(userId).orElseThrow(() -> new NotFoundUserException("id", userId));
        Car car = carDAO.findById(carId).orElseThrow(() -> new NotFoundCarException("id",carId));
        Rent rentTemp = modelMapper.map(createRentDTO,Rent.class);
        rentTemp.setCar(car);
        rentTemp.setToken(generateToken());
        rentTemp.setPrice(car.getPrice()* Period.between(createRentDTO.getRentStartedAt().toLocalDate(),createRentDTO.getRentEndsAt().toLocalDate()).getDays());
        List<Rent> rents = user.getRents();
        rents.add(rentTemp);

        user.setRents(rents);

        Rent rent = userDAO.save(user).getRents().stream().reduce((f,s) -> s).get();

        return modelMapper.map(rent,RentDTO.class);
        //return modelMapper.map(rentDAO.save(rent),RentDTO.class);
    }

    @Override
    public RentDTO getRentById(long id) {
        return modelMapper.map(rentDAO.findById(id).orElseThrow(() ->new NotFoundRentException("id",id)),RentDTO.class);
    }

    @Override
    public List<RentDTO> getRents() {
        return modelMapper.map(rentDAO.findAll(),targetListType);
    }

    @Override
    public List<RentDTO> getActiveRents() {
        return modelMapper.map(rentDAO.findRentsByActive(true),targetListType);
    }

    @Override
    public List<RentDTO> getRentsByUserId(long userId) {
        UserDTO user = modelMapper.map(userDAO.findById(userId).orElseThrow(() -> new NotFoundUserException("id", userId)),UserDTO.class);
        return user.getRents();
    }

    @Override
    public List<RentDTO> getActiveRentsByUserId(long userId) {
        UserDTO user = modelMapper.map(userDAO.findById(userId).orElseThrow(() -> new NotFoundUserException("id", userId)),UserDTO.class);
        return user.getRents().stream().filter(x -> x.isActive()).collect(Collectors.toList());
    }

    @Override
    public void deleteRentById(long id) {
        Rent rent = rentDAO.findById(id).orElseThrow(() -> new NotFoundRentException("id", id));
        rentDAO.delete(rent);
    }

    @Override
    public String generateToken() {
        int length = 5;
        boolean useLetters = true;
        boolean useNumbers = true;
        String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
        generatedString = LocalDate.now().toString().concat(generatedString);
        Optional<Rent> optionalRent = rentDAO.findRentByToken(generatedString);
        if (!optionalRent.isPresent()){
            return generatedString;
        } else {
            return generateToken();
        }
    }

    @Override
    public void setRentActive(long id) {
        Rent rent = rentDAO.findById(id).orElseThrow(() -> new NotFoundRentException("id",id));
        rent.setActive(true);
        rentDAO.save(rent);
    }

    @Override
    public void setRentInactive(long id) {
        Rent rent = rentDAO.findById(id).orElseThrow(() -> new NotFoundRentException("id",id));
        rent.setActive(false);
        rentDAO.save(rent);
    }

    @Override
    public boolean areRentDatesInvalid(DateIntervalDTO dateIntervalDTO) {
        LocalDateTime todayWithZeros = LocalDate.now().atTime(0,0);
        if (dateIntervalDTO.getDateEnd().isBefore(dateIntervalDTO.getDateStart()) || dateIntervalDTO.getDateStart().isBefore(todayWithZeros)){
            return true;
        }
        return false;
    }
}
