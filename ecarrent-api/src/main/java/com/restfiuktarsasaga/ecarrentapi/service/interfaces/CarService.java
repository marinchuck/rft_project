package com.restfiuktarsasaga.ecarrentapi.service.interfaces;

import com.restfiuktarsasaga.ecarrentapi.dto.car.CarDTO;
import com.restfiuktarsasaga.ecarrentapi.dto.car.CreateCarDTO;
import com.restfiuktarsasaga.ecarrentapi.entity.Car;

import java.time.LocalDateTime;
import java.util.List;

public interface CarService {

    CarDTO createCar(CreateCarDTO car, long parkId);

    CarDTO getCarById(long id);

    List<CarDTO> getCars();

    List<Car> getCarsByMake(String make);

    List<Car> getCarsByModel(String model);

    List<Car> getCarsByType(String type);

    List<Car> getCarsByColor(String color);

    List<CarDTO> getCarsByPark(long parkId);

    void deleteCarByRegistrationNumber(String number);

    void deleteCarById(long id);

    List<CarDTO> getCarsByPlaceAndDate(long parkId, LocalDateTime dateStart, LocalDateTime dateEnd);
}
