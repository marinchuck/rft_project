package com.restfiuktarsasaga.ecarrentapi.service.interfaces;

import com.restfiuktarsasaga.ecarrentapi.dto.user.ChangePasswordDTO;
import com.restfiuktarsasaga.ecarrentapi.dto.user.CreateUserDTO;
import com.restfiuktarsasaga.ecarrentapi.dto.user.UserDTO;
import com.restfiuktarsasaga.ecarrentapi.entity.RegistrationToken;
import com.restfiuktarsasaga.ecarrentapi.entity.User;

import java.util.List;

public interface UserService {

    UserDTO createUser(CreateUserDTO userDTO);

    UserDTO getUserById(long id);

    User getNormalUserById(long id);

    List<UserDTO> getUsers();

    void deleteUserByUsername(String userName);

    void deleteUserById(long id);

    UserDTO saveUser(User user);

    RegistrationToken createRegistrationToken(User user, String token);

    UserDTO changePassword(ChangePasswordDTO userData);
}
