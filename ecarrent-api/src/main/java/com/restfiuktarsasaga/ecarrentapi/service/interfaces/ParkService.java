package com.restfiuktarsasaga.ecarrentapi.service.interfaces;

import com.restfiuktarsasaga.ecarrentapi.dto.park.CreateParkDTO;
import com.restfiuktarsasaga.ecarrentapi.dto.park.ParkDTO;
import com.restfiuktarsasaga.ecarrentapi.entity.Car;
import com.restfiuktarsasaga.ecarrentapi.entity.Park;

import java.util.List;


public interface ParkService {

    ParkDTO createPark(CreateParkDTO parkDTO);

    ParkDTO getParkById(long id);

    List<ParkDTO> getParks();

    List<Park> getParksByCountry(String country);

    List<Park> getParksByCity(String city);

    void deleteParkById(long id);

    Park getParkByCar(Car car);
}
