package com.restfiuktarsasaga.ecarrentapi.service;

import com.restfiuktarsasaga.ecarrentapi.dao.CarDAO;
import com.restfiuktarsasaga.ecarrentapi.dao.ParkDAO;
import com.restfiuktarsasaga.ecarrentapi.dao.RentDAO;
import com.restfiuktarsasaga.ecarrentapi.dto.car.CarDTO;
import com.restfiuktarsasaga.ecarrentapi.dto.car.CreateCarDTO;
import com.restfiuktarsasaga.ecarrentapi.entity.Car;
import com.restfiuktarsasaga.ecarrentapi.entity.Park;
import com.restfiuktarsasaga.ecarrentapi.entity.Rent;
import com.restfiuktarsasaga.ecarrentapi.exception.CarCantBeDeletedException;
import com.restfiuktarsasaga.ecarrentapi.exception.NotFoundCarException;
import com.restfiuktarsasaga.ecarrentapi.exception.NotFoundParkException;
import com.restfiuktarsasaga.ecarrentapi.service.interfaces.CarService;
import com.restfiuktarsasaga.ecarrentapi.service.interfaces.ParkService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Data
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {

    private final CarDAO carDAO;

    private final ParkDAO parkDAO;

    private final RentDAO rentDAO;

    private final ParkService parkService;

    private final ModelMapper modelMapper;

    private static Type targetListType = new TypeToken<List<CarDTO>>() {}.getType();

    @Override
    public CarDTO createCar(CreateCarDTO carDTO, long parkId) {
        Park park = parkDAO.findById(parkId).orElseThrow(() -> new NotFoundParkException("id", parkId));
        List<Car> cars = park.getCars();
        cars.add(modelMapper.map(carDTO,Car.class));
        park.setCars(cars);
        //Car car = carDAO.save(modelMapper.map(carDTO,Car.class));
        Car car = parkDAO.save(park).getCars().stream().reduce((f,s) -> s).get();
        return modelMapper.map(car,CarDTO.class);
    }

    @Override
    public CarDTO getCarById(long id) {
        return modelMapper.map(carDAO.findById(id).orElseThrow(() -> new NotFoundCarException("id",id)),CarDTO.class);
    }

    @Override
    public List<CarDTO> getCars() {
        return modelMapper.map(carDAO.findAll(), targetListType);
    }

    @Override
    public List<Car> getCarsByMake(String make) {
        return modelMapper.map(carDAO.findCarsByMake(make),targetListType);
    }

    @Override
    public List<Car> getCarsByModel(String model) {

        return modelMapper.map(carDAO.findCarsByModel(model),targetListType);
    }

    @Override
    public List<Car> getCarsByType(String type) {

        return modelMapper.map(carDAO.findCarsByType(type),targetListType);
    }

    @Override
    public List<Car> getCarsByColor(String color) {
        return modelMapper.map(carDAO.findCarsByColor(color),targetListType);
    }

    @Override
    public List<CarDTO> getCarsByPark(long parkId) {
        List<Car> cars = parkDAO.findById(parkId).orElseThrow(() -> new NotFoundParkException("id",parkId)).getCars();
        return modelMapper.map(cars, targetListType);
    }

    @Override
    public void deleteCarByRegistrationNumber(String number) {
        Optional<Car> car = carDAO.findCarByRegistrationNumber(number);
        if (!car.isPresent()){
            throw new NotFoundCarException("Rendszám", number);
        }
        Car carToDelete = car.get();
        Park park = parkService.getParkByCar(carToDelete);
        park.getCars().remove(carToDelete);
        parkDAO.save(park);
        carDAO.delete(carToDelete);
    }

    @Override
    public void deleteCarById(long id) {
        Car car = carDAO.findById(id).orElseThrow(() -> new NotFoundCarException("id", id));

        if (!isCarDeletable(car)){
            throw new CarCantBeDeletedException(Long.toString(id));
        }

        carDAO.delete(car);
    }

    private boolean isCarDeletable(Car car){

        List<Rent> rents = rentDAO.findRentsByCar(car);
        List<Rent> rentsToCome = new ArrayList<>();
        rentsToCome = rents.stream()
                .filter(rent -> !rent.getRentStartedAt().isAfter(LocalDateTime.now())
                        && !rent.getRentEndsAt().isAfter(LocalDateTime.now()))
                .collect(Collectors.toList());
        return rentsToCome.isEmpty();
    }

    @Override
    public List<CarDTO> getCarsByPlaceAndDate(long parkId, LocalDateTime dateStart, LocalDateTime dateEnd) {
        //az adott parkban lévő autók.
        List<CarDTO> carDTOsInPark = getCarsByPark(parkId);

        //autók amelyeknek ütközik valamely foglalása a megadott intervallummal.
        List<Car> carsNotAvailable = rentDAO.findAll().stream()
                .filter(x -> !x.getRentStartedAt().isAfter(dateEnd) && !x.getRentEndsAt().isBefore(dateStart))
                .map(x -> x.getCar()).distinct().collect(Collectors.toList());
        List<CarDTO> carDTOsNotAvailable = modelMapper.map(carsNotAvailable,targetListType);

        //autók amelyek az adott parkban vannak és nem ütköznek egyetlen már a rendszerben lévő foglalással sem.
        List<CarDTO> carDTOsAvailable = carDTOsInPark.stream().filter(x -> !carDTOsNotAvailable
                .stream().collect(Collectors.toSet()).contains(x)).collect(Collectors.toList());

        return carDTOsAvailable;
    }


}
