package com.restfiuktarsasaga.ecarrentapi.service;

import com.restfiuktarsasaga.ecarrentapi.dao.PictureDAO;
import com.restfiuktarsasaga.ecarrentapi.dto.picture.PictureDTO;
import com.restfiuktarsasaga.ecarrentapi.entity.Picture;
import com.restfiuktarsasaga.ecarrentapi.exception.FileStorageException;
import com.restfiuktarsasaga.ecarrentapi.exception.NotFoundPictrureException;
import com.restfiuktarsasaga.ecarrentapi.exception.PictureStorageException;
import com.restfiuktarsasaga.ecarrentapi.service.interfaces.PictureService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
@Data
@RequiredArgsConstructor
public class PictureServiceImpl implements PictureService {

    private final PictureDAO pictureDAO;

    private final ModelMapper modelMapper;

    @Override
    public PictureDTO savePicture(MultipartFile file) {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        String fileType = file.getContentType();
        long fileSize = file.getSize() / 1000000;
        try {
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }
            if (fileSize > 0.8){
                throw new FileStorageException("Sorry! Too large file " + fileSize + "MB. (maximum is 800KB)" );
            }
            assert fileType != null;
            if(!(fileType.equals("image/png") || fileType.equals("image/jpeg"))){
                throw new PictureStorageException("Sorry! Not valid file type " + fileType);
            }

            Picture picture = new Picture(fileName, fileType, file.getBytes());
            new Picture();

            return modelMapper.map(pictureDAO.save(picture),PictureDTO.class);
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!");
        }
    }

    @Override
    public PictureDTO getPicture(long id) {
        return modelMapper.map(pictureDAO.findById(id).orElseThrow(() -> new NotFoundPictrureException("id",id)),PictureDTO.class);
    }

    @Override
    public void deletePictureById(long id) {
        Picture picture = pictureDAO.findById(id).orElseThrow(() -> new NotFoundPictrureException("id",id));
        pictureDAO.delete(picture);
    }
}
