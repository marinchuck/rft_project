package com.restfiuktarsasaga.ecarrentapi.service;

import com.restfiuktarsasaga.ecarrentapi.dao.ParkDAO;
import com.restfiuktarsasaga.ecarrentapi.dto.park.CreateParkDTO;
import com.restfiuktarsasaga.ecarrentapi.dto.park.ParkDTO;
import com.restfiuktarsasaga.ecarrentapi.entity.Car;
import com.restfiuktarsasaga.ecarrentapi.entity.Park;
import com.restfiuktarsasaga.ecarrentapi.exception.NotFoundParkException;
import com.restfiuktarsasaga.ecarrentapi.exception.ParkCantBeDeletedException;
import com.restfiuktarsasaga.ecarrentapi.service.interfaces.ParkService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Service
@Data
@RequiredArgsConstructor
public class ParkServiceImpl implements ParkService {

    private final ParkDAO parkDAO;

    private final ModelMapper modelMapper;

    private static Type targetListType = new TypeToken<List<ParkDTO>>(){}.getType();

    @Override
    public ParkDTO createPark(CreateParkDTO parkDTO) {
        Park park = parkDAO.save(modelMapper.map(parkDTO,Park.class));
        return modelMapper.map(park,ParkDTO.class);
    }

    @Override
    public ParkDTO getParkById(long id) {
        return modelMapper.map(parkDAO.findById(id).orElseThrow(() -> new NotFoundParkException("id",id)), ParkDTO.class);
    }

    @Override
    public List<ParkDTO> getParks() {
        return modelMapper.map(parkDAO.findAll(), targetListType);
    }

    @Override
    public List<Park> getParksByCountry(String country) {
        return modelMapper.map(parkDAO.findParksByCountry(country),targetListType);
    }

    @Override
    public List<Park> getParksByCity(String city) {
        return modelMapper.map(parkDAO.findParksByCity(city),targetListType);
    }

    @Override
    public void deleteParkById(long id) {
        Park park = parkDAO.findById(id).orElseThrow(() -> new NotFoundParkException("id", id));
        if (!park.getCars().isEmpty())
            throw new ParkCantBeDeletedException(Long.toString(id));

        parkDAO.delete(park);
    }

    @Override
    public Park getParkByCar(Car car) {
        List<ParkDTO> allParks = getParks();
        List<Park> parks = new ArrayList<>();
        allParks.forEach(parkDTO -> parks.add(modelMapper.map(parkDTO,Park.class)));

        for (Park park : parks) {
            if (park.getCars().contains(car)){
                return park;
            }
        }
        return null;
    }
}
