package com.restfiuktarsasaga.ecarrentapi.service;

import com.restfiuktarsasaga.ecarrentapi.dao.AuthorityDAO;
import com.restfiuktarsasaga.ecarrentapi.dao.RegistrationTokenDAO;
import com.restfiuktarsasaga.ecarrentapi.dao.UserDAO;
import com.restfiuktarsasaga.ecarrentapi.dto.user.ChangePasswordDTO;
import com.restfiuktarsasaga.ecarrentapi.dto.user.CreateUserDTO;
import com.restfiuktarsasaga.ecarrentapi.dto.user.UserDTO;
import com.restfiuktarsasaga.ecarrentapi.entity.Authority;
import com.restfiuktarsasaga.ecarrentapi.entity.RegistrationToken;
import com.restfiuktarsasaga.ecarrentapi.entity.User;
import com.restfiuktarsasaga.ecarrentapi.exception.NotFoundUserException;
import com.restfiuktarsasaga.ecarrentapi.service.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserDAO userDAO;

    private final AuthorityDAO authorityDAO;

    private final ModelMapper modelMapper;

    private static Type targetListType = new TypeToken<List<UserDTO>>(){}.getType();

    private final RegistrationTokenDAO registrationTokenDAO;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public UserDTO createUser(CreateUserDTO userDTO) {
        String password =  passwordEncoder.encode(userDTO.getHashedPass());
        userDTO.setHashedPass(password);
        Authority auth = authorityDAO.findAuthorityByAuthority("ROLE_ECARRENT_USER").get();

        User user = modelMapper.map(userDTO,User.class);
        user.setAuthorities(Arrays.asList(auth));
        user.setEnabled(false);
        User userToReturn = userDAO.save(user);
        return modelMapper.map(userToReturn, UserDTO.class);
    }

    @Override
    public UserDTO getUserById(long id) {
        return modelMapper.map(userDAO.findById(id).orElseThrow(() ->new NotFoundUserException("id", id)),UserDTO.class);
    }

    @Override
    public List<UserDTO> getUsers() {
        return modelMapper.map(userDAO.findAll(), targetListType);
    }

    @Override
    public void deleteUserByUsername(String name) {
        Optional<User> user = userDAO.findUserByUsername(name);
        if (!user.isPresent()){
            throw new NotFoundUserException("username", name);
        }
        deleteUserById(user.get().getId());
    }

    @Override
    public void deleteUserById(long id) {
        User user = userDAO.findById(id).orElseThrow(() -> new NotFoundUserException("id",id));
        userDAO.delete(user);
    }

    @Override
    public UserDTO saveUser(User user) {
        User saved = userDAO.save(user);
        return modelMapper.map(saved, UserDTO.class);
    }

    @Override
    public RegistrationToken createRegistrationToken(User user, String token) {
        RegistrationToken registrationToken = new RegistrationToken(token,user);
        return registrationTokenDAO.save(registrationToken);
    }

    @Override
    public User getNormalUserById(long id) {
        return userDAO.findById(id).orElseThrow(() -> new NotFoundUserException("id",id));
    }

    @Override
    public UserDTO changePassword(ChangePasswordDTO userData) {
        User user = userDAO.findById(userData.getId()).orElseThrow(() -> new NotFoundUserException("id",userData.getId()));
        if (!passwordEncoder.matches(userData.getOldPassword(),user.getHashedPass())){
            return null;
        }

        user.setHashedPass(passwordEncoder.encode(userData.getNewPassword()));
        return modelMapper.map(userDAO.save(user),UserDTO.class);
    }
}
