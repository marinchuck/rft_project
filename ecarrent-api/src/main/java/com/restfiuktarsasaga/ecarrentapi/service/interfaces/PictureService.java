package com.restfiuktarsasaga.ecarrentapi.service.interfaces;

import com.restfiuktarsasaga.ecarrentapi.dto.picture.PictureDTO;
import org.springframework.web.multipart.MultipartFile;

public interface PictureService {

    PictureDTO savePicture(MultipartFile file);

    PictureDTO getPicture(long id);

    void deletePictureById(long id);
}
