package com.restfiuktarsasaga.ecarrentapi.service.interfaces;

import com.restfiuktarsasaga.ecarrentapi.dto.dateinterval.DateIntervalDTO;
import com.restfiuktarsasaga.ecarrentapi.dto.rent.CreateRentDTO;
import com.restfiuktarsasaga.ecarrentapi.dto.rent.RentDTO;

import java.util.List;

public interface RentService {

    RentDTO createRent(CreateRentDTO rentDTO, long carId, long userId);

    RentDTO getRentById(long id);

    List<RentDTO> getRents();

    List<RentDTO> getActiveRents();

    List<RentDTO> getRentsByUserId(long userId);

    List<RentDTO> getActiveRentsByUserId(long userId);

    void deleteRentById(long id);

    String generateToken();

    void setRentActive(long id);

    void setRentInactive(long id);

    boolean areRentDatesInvalid(DateIntervalDTO dateDTO);
}
