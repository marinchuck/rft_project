package com.restfiuktarsasaga.ecarrentapi.validators.car;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = RegNumberValidator.class)
public @interface ValidRegNumber {

    String message() default "Please enter a valid registration number";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
