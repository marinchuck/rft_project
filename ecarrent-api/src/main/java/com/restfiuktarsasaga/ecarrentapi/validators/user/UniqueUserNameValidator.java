package com.restfiuktarsasaga.ecarrentapi.validators.user;

import com.restfiuktarsasaga.ecarrentapi.dao.UserDAO;
import com.restfiuktarsasaga.ecarrentapi.entity.User;
import lombok.RequiredArgsConstructor;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

@RequiredArgsConstructor
public class UniqueUserNameValidator implements ConstraintValidator<UniqueUserName, String> {

    private final UserDAO userDAO;

    @Override
    public boolean isValid(String username, ConstraintValidatorContext constraintValidatorContext) {
        Optional<User> user = userDAO.findUserByUsername(username);
        return !user.isPresent();
    }

    @Override
    public void initialize(UniqueUserName constraintAnnotation) {

    }
}
