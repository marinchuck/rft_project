package com.restfiuktarsasaga.ecarrentapi.validators.car;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegNumberValidator implements ConstraintValidator<ValidRegNumber, String> {

    private Pattern pattern;
    private Matcher matcher;
    private static final String REG_NUMBER_PATTERN = "^[A-Z]{3}-[0-9]{3}$";

    @Override
    public boolean isValid(String regNumber, ConstraintValidatorContext constraintValidatorContext) {
        return validateRegNumber(regNumber);
    }

    @Override
    public void initialize(ValidRegNumber constraintAnnotation) {

    }

    private boolean validateRegNumber(String regNumber){
        pattern = Pattern.compile(REG_NUMBER_PATTERN);
        matcher = pattern.matcher(regNumber);
        return matcher.matches();
    }
}
