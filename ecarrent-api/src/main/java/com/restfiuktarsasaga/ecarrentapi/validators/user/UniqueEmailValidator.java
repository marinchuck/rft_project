package com.restfiuktarsasaga.ecarrentapi.validators.user;

import com.restfiuktarsasaga.ecarrentapi.dao.UserDAO;
import com.restfiuktarsasaga.ecarrentapi.entity.User;
import lombok.RequiredArgsConstructor;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

@RequiredArgsConstructor
public class UniqueEmailValidator implements ConstraintValidator<UniqueEmail, String> {

    private final UserDAO userDAO;

    @Override
    public boolean isValid(String email, ConstraintValidatorContext constraintValidatorContext) {
        Optional<User> user =  userDAO.findUserByEmail(email);
        return !user.isPresent();
    }

    @Override
    public void initialize(UniqueEmail constraintAnnotation) {

    }
}
