package com.restfiuktarsasaga.ecarrentapi.exception;

public class NotFoundUserException extends NotFoundResourceException {

    public NotFoundUserException(String fieldName, Object fieldValue) {
        super("Felhasználó", fieldName, fieldValue);
    }
}
