package com.restfiuktarsasaga.ecarrentapi.exception;

public class NotFoundParkException extends NotFoundResourceException {

    public NotFoundParkException(String fieldName, Object fieldValue) {
        super("Park", fieldName, fieldValue);
    }
}
