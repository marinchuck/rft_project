package com.restfiuktarsasaga.ecarrentapi.exception;

public class PictureStorageException extends FileStorageException {
    public PictureStorageException(String message) {
        super(message);
    }
}
