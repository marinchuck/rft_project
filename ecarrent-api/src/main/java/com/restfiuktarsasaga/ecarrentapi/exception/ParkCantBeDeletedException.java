package com.restfiuktarsasaga.ecarrentapi.exception;

public class ParkCantBeDeletedException extends RuntimeException {

    public ParkCantBeDeletedException(String id) {
        super("Park can't be deleted with id: " + id);
    }
}
