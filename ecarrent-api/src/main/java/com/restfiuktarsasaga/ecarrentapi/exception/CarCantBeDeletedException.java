package com.restfiuktarsasaga.ecarrentapi.exception;

public class CarCantBeDeletedException extends RuntimeException {

    public CarCantBeDeletedException(String id) {
        super("Car can't be deleted with id: " + id);
    }
}
