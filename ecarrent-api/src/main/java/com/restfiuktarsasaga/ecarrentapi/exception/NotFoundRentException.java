package com.restfiuktarsasaga.ecarrentapi.exception;

public class NotFoundRentException extends NotFoundResourceException {

    public NotFoundRentException(String fieldName, Object fieldValue) {
        super("Foglalás", fieldName, fieldValue);
    }
}
