package com.restfiuktarsasaga.ecarrentapi.exception;

public class NotFoundCarException extends NotFoundResourceException {

    public NotFoundCarException(String fieldName, Object fieldValue) {
        super("Autó", fieldName, fieldValue);
    }
}
