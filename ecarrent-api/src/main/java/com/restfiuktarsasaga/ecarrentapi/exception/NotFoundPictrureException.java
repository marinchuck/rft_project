package com.restfiuktarsasaga.ecarrentapi.exception;

public class NotFoundPictrureException extends NotFoundResourceException {
    public NotFoundPictrureException(String fieldName, Object fieldValue) {
        super("Kép", fieldName, fieldValue);
    }
}
