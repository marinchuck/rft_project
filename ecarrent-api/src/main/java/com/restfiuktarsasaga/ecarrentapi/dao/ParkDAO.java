package com.restfiuktarsasaga.ecarrentapi.dao;

import com.restfiuktarsasaga.ecarrentapi.entity.Park;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ParkDAO extends JpaRepository<Park, Long> {

    List<Park> findParksByCity(String city);

    List<Park> findParksByCountry(String country);
}