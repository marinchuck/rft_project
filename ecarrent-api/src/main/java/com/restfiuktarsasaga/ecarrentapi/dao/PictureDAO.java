package com.restfiuktarsasaga.ecarrentapi.dao;

import com.restfiuktarsasaga.ecarrentapi.entity.Picture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PictureDAO extends JpaRepository<Picture, Long> {
}
