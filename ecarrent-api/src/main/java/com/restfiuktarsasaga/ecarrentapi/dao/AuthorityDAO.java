package com.restfiuktarsasaga.ecarrentapi.dao;

import com.restfiuktarsasaga.ecarrentapi.entity.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthorityDAO extends JpaRepository<Authority,Long>{

    Optional<Authority> findAuthorityByAuthority(String authority);
}
