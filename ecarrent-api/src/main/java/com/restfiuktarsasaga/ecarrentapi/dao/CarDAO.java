package com.restfiuktarsasaga.ecarrentapi.dao;

import com.restfiuktarsasaga.ecarrentapi.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CarDAO extends JpaRepository<Car, Long> {

    List<Car> findCarsByColor(String color);

    List<Car> findCarsByMake(String make);

    List<Car> findCarsByModel(String model);

    List<Car> findCarsByType(String type);

    Optional<Car> findCarByRegistrationNumber(String number);
}