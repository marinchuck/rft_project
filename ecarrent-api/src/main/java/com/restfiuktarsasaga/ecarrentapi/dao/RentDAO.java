package com.restfiuktarsasaga.ecarrentapi.dao;

import com.restfiuktarsasaga.ecarrentapi.entity.Car;
import com.restfiuktarsasaga.ecarrentapi.entity.Rent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RentDAO extends JpaRepository<Rent, Long> {

    List<Rent> findRentsByActive(boolean active);

    List<Rent> findRentsByCar(Car car);

    Optional<Rent> findRentByToken(String token);

    List<Rent> findRentsByCarAndActive(Car car, boolean active);
}
