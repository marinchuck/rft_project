package com.restfiuktarsasaga.ecarrentapi.dao;

import com.restfiuktarsasaga.ecarrentapi.entity.RegistrationToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RegistrationTokenDAO extends JpaRepository<RegistrationToken,Long> {

    Optional<RegistrationToken> findTokenByToken(String token);
}
