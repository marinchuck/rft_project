package com.restfiuktarsasaga.ecarrentapi.enums;

public enum Gearbox {
    MANUAL, AUTOMATIC
}
