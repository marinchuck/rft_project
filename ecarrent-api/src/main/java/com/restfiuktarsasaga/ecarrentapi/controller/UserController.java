package com.restfiuktarsasaga.ecarrentapi.controller;

import com.restfiuktarsasaga.ecarrentapi.dao.RegistrationTokenDAO;
import com.restfiuktarsasaga.ecarrentapi.dto.user.ChangePasswordDTO;
import com.restfiuktarsasaga.ecarrentapi.dto.user.CreateUserDTO;
import com.restfiuktarsasaga.ecarrentapi.dto.user.UserDTO;
import com.restfiuktarsasaga.ecarrentapi.entity.RegistrationToken;
import com.restfiuktarsasaga.ecarrentapi.entity.User;
import com.restfiuktarsasaga.ecarrentapi.events.OnRegistrationCompleteEvent;
import com.restfiuktarsasaga.ecarrentapi.service.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    private final ApplicationEventPublisher applicationEventPublisher;

    private final RegistrationTokenDAO registrationTokenDAO;

    @PostMapping("/register")
    public ResponseEntity<?> createUser(@Valid @RequestBody CreateUserDTO userDTO, WebRequest request){

        UserDTO user = userService.createUser(userDTO);
        User registeredUser = userService.getNormalUserById(user.getId());
        try {
            String appUrl = request.getContextPath();
            applicationEventPublisher.publishEvent(new OnRegistrationCompleteEvent(registeredUser, appUrl));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Nem küldhető el az email.");
        }
        return ResponseEntity.ok().body(user);
    }

    @GetMapping("/registrationConfirm")
    public void confirmRegistration(@RequestParam(value = "token") String token, HttpServletResponse response, WebRequest request) throws IOException {
        Optional<RegistrationToken> registrationToken = registrationTokenDAO.findTokenByToken(token);
        if (!registrationToken.isPresent()){
//            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("A token nem található.");
            response.sendRedirect("http://localhost:4200/login" + "?success=noToken");
        }
        else {
            RegistrationToken regToken = registrationToken.get();
            if (regToken.getExpiryDate().isBefore(LocalDateTime.now())) {
//            return ResponseEntity.status(HttpStatus.REQUEST_TIMEOUT).body("A token sajnos lejárt.");

                User registeredUser = regToken.getUser();
                try {
                    String appUrl = request.getContextPath();
                    applicationEventPublisher.publishEvent(new OnRegistrationCompleteEvent(registeredUser, appUrl));

                } catch (Exception e) {

                }

                response.sendRedirect("http://localhost:4200/login" + "?success=fail");
            } else {

                User user = regToken.getUser();
                user.setEnabled(true);
                UserDTO userDTO = userService.saveUser(user);
                response.sendRedirect("http://localhost:4200/login" + "?success=success");

            }
        }
//        return ResponseEntity.ok().body(userDTO);
    }

    @GetMapping()
    public List<UserDTO> getAllUsers() {
        return userService.getUsers();
    }

    @GetMapping("/{id}")
    public UserDTO getUserById(@PathVariable(value = "id") Long userId){
        return userService.getUserById(userId);
    }

//    @DeleteMapping("/{name}")
//    @PreAuthorize("hasRole('ECARRENT_ADMIN')")
//    public ResponseEntity<?> deleteUserByUsername(@PathVariable(name = "name") String name){
//        userService.deleteUserByUsername(name);
//        return ResponseEntity.ok().build();
//    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ECARRENT_ADMIN')")
    public ResponseEntity<?> deleteUserById(@PathVariable(name = "id") long id){
        userService.deleteUserById(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/changePassword")
    @PreAuthorize("hasRole('ECARRENT_USER')")
    public ResponseEntity<?> changePassword(@RequestBody ChangePasswordDTO userData){
        UserDTO user = userService.changePassword(userData);
        if (user == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Hibás régi jelszó!");
        }
        return ResponseEntity.ok().body(user);
    }
}
