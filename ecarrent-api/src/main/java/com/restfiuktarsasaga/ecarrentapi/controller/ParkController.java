package com.restfiuktarsasaga.ecarrentapi.controller;


import com.restfiuktarsasaga.ecarrentapi.dto.park.CreateParkDTO;
import com.restfiuktarsasaga.ecarrentapi.dto.park.ParkDTO;
import com.restfiuktarsasaga.ecarrentapi.entity.Park;
import com.restfiuktarsasaga.ecarrentapi.exception.ParkCantBeDeletedException;
import com.restfiuktarsasaga.ecarrentapi.service.interfaces.ParkService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/parks")
@RequiredArgsConstructor
public class ParkController {

    private final ParkService parkService;

    @PostMapping()
    @PreAuthorize("hasRole('ECARRENT_ADMIN')")
    public ResponseEntity<ParkDTO> createPark(@Valid @RequestBody CreateParkDTO parkDTO){
        ParkDTO park = parkService.createPark(parkDTO);
        return ResponseEntity.ok().body(park);
    }

    @GetMapping()
    public List<ParkDTO> getAllParks(){return parkService.getParks();}

    @GetMapping("/{id}")
    public ParkDTO getParkById(@PathVariable(value = "id") long id) {return parkService.getParkById(id);}

    @GetMapping("/city/{city}")
    public List<Park> getParksByCity(@PathVariable(value = "city") String city) {return parkService.getParksByCity(city);}

    @GetMapping("/country/{country}")
    public List<Park> getParksByCountry(@PathVariable(value = "country") String country) {return parkService.getParksByCountry(country);}

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ECARRENT_ADMIN')")
    public ResponseEntity<?> deleteParkById(@PathVariable(name = "id") long id){
        parkService.deleteParkById(id);
        return ResponseEntity.ok().build();
    }

    @ExceptionHandler(ParkCantBeDeletedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<String> parkCantBeDeletedHandler(Exception exception){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
    }
}
