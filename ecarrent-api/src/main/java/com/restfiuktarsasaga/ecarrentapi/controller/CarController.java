package com.restfiuktarsasaga.ecarrentapi.controller;


import com.restfiuktarsasaga.ecarrentapi.dto.car.CarDTO;
import com.restfiuktarsasaga.ecarrentapi.dto.car.CreateCarDTO;
import com.restfiuktarsasaga.ecarrentapi.dto.dateinterval.DateIntervalDTO;
import com.restfiuktarsasaga.ecarrentapi.entity.Car;
import com.restfiuktarsasaga.ecarrentapi.exception.CarCantBeDeletedException;
import com.restfiuktarsasaga.ecarrentapi.service.interfaces.CarService;
import com.restfiuktarsasaga.ecarrentapi.service.interfaces.RentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/cars")
@RequiredArgsConstructor
public class CarController {

    private final CarService carService;

    private final RentService rentService;

    @PostMapping("/parks/{parkId}")
    @PreAuthorize("hasRole('ECARRENT_ADMIN')")
    public ResponseEntity<CarDTO> createCar(@PathVariable(name = "parkId") long parkId, @Valid @RequestBody CreateCarDTO carDTO){
        CarDTO car = carService.createCar(carDTO,parkId);
        return ResponseEntity.ok().body(car);
    }

    @GetMapping()
    @PreAuthorize("hasRole('ECARRENT_USER')")
    public List<CarDTO> getAllCars(){
        return carService.getCars();
    }

    @GetMapping("/{id}")
    public CarDTO getCarById(@PathVariable(value = "id") long carId){
        return carService.getCarById(carId);
    }

    @GetMapping("/make/{make}")
    public List<Car> getCarByMake(@PathVariable(value = "make") String make){
        return carService.getCarsByMake(make);
    }

    @GetMapping("/model/{model}")
    public List<Car> getCarByModel(@PathVariable(value = "model") String model){
        return carService.getCarsByModel(model);
    }
    @GetMapping("/type/{type}")
    public List<Car> getCarByType(@PathVariable(value = "type") String type){
        return carService.getCarsByType(type);
    }

    @GetMapping("/color/{color}")
    public List<Car> getCarByColor(@PathVariable(value = "color") String color){
        return carService.getCarsByColor(color);
    }

    @GetMapping("/parks/{parkId}")
    public ResponseEntity<?> getCarsByPlaceAndDate(@PathVariable(value = "parkId") long parkId, @RequestParam(value = "dateStart") String dateStart, @RequestParam(value = "dateEnd") String dateEnd){
        LocalDateTime dateS = LocalDateTime.parse(dateStart);
        LocalDateTime dateE = LocalDateTime.parse(dateEnd);
        DateIntervalDTO dateIntervalDTO = new DateIntervalDTO();
        dateIntervalDTO.setDateStart(dateS);
        dateIntervalDTO.setDateEnd(dateE);
        if (rentService.areRentDatesInvalid(dateIntervalDTO)){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Hibás dátumok");
        }
        List<CarDTO> cars = carService.getCarsByPlaceAndDate(parkId, dateIntervalDTO.getDateStart(),dateIntervalDTO.getDateEnd());
        return ResponseEntity.ok().body(cars);
    }

    @DeleteMapping("/{number}")
    @PreAuthorize("hasRole('ECARRENT_ADMIN')")
    public ResponseEntity<?> deleteCarByRegistrationNumber(@PathVariable(value = "number") String number){
        carService.deleteCarByRegistrationNumber(number);
        return ResponseEntity.ok().build();
    }

    @ExceptionHandler(CarCantBeDeletedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<String> carCantBeDeletedHandler(Exception exception){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
    }
}
