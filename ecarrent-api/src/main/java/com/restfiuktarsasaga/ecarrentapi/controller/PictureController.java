package com.restfiuktarsasaga.ecarrentapi.controller;

import com.restfiuktarsasaga.ecarrentapi.dto.picture.PictureDTO;
import com.restfiuktarsasaga.ecarrentapi.dto.picture.PictureDownloadDTO;
import com.restfiuktarsasaga.ecarrentapi.service.interfaces.PictureService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import javax.xml.bind.DatatypeConverter;
import java.util.Base64;

@RestController
@RequestMapping("/pictures")
@RequiredArgsConstructor
public class PictureController {

    private final PictureService pictureService;

    @PostMapping()
    public ResponseEntity<String> uploadPicture(@RequestParam("file")MultipartFile file){
        PictureDTO pictureDTO = pictureService.savePicture(file);

        String downloadURL = ServletUriComponentsBuilder.fromCurrentContextPath().path("/pictures/").path(pictureDTO.getId().toString()).toUriString();

        return ResponseEntity.ok().body(downloadURL);
    }

    @GetMapping("/{id}")
    public ResponseEntity<byte[]> downloadPicture(@PathVariable long id){
        PictureDTO pictureDTO = pictureService.getPicture(id);
        PictureDownloadDTO picture = new PictureDownloadDTO();

        picture.setData(Base64.getEncoder().encodeToString(new ByteArrayResource(pictureDTO.getData()).getByteArray()));

//        System.out.println(Base64.getEncoder().encode(pictureDTO.getData()));
        picture.setData(DatatypeConverter.printBase64Binary(pictureDTO.getData()));

        

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(pictureDTO.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + pictureDTO.getFilename() + "\"")
//                .body(Base64.getEncoder().encode(new ByteArrayResource(pictureDTO.getData()).getByteArray()));
        .body(new ByteArrayResource(pictureDTO.getData()).getByteArray());

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePictureById(@PathVariable(name = "id") long id){
        pictureService.deletePictureById(id);
        return ResponseEntity.ok().build();
    }
}
