package com.restfiuktarsasaga.ecarrentapi.controller;

import com.restfiuktarsasaga.ecarrentapi.dto.rent.CreateRentDTO;
import com.restfiuktarsasaga.ecarrentapi.dto.rent.RentDTO;
import com.restfiuktarsasaga.ecarrentapi.service.interfaces.RentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/rentals")
@RequiredArgsConstructor
public class RentController {

    private final RentService rentService;

    @PostMapping("/cars/{carId}/users/{userId}")
    @PreAuthorize("hasRole('ECARRENT_USER')")
    public ResponseEntity<RentDTO> createRent(@PathVariable(name = "carId") long carId, @PathVariable(name = "userId") long userId, @Valid @RequestBody CreateRentDTO rentDTO){
        RentDTO rent = rentService.createRent(rentDTO,carId,userId);
        return ResponseEntity.ok().body(rent);
    }

    @GetMapping()
    public List<RentDTO> getAllRentals() {
        return rentService.getRents();
    }

    @GetMapping("/active")
    @PreAuthorize("hasRole('ECARRENT_ADMIN')")
    public List<RentDTO> getActiveRentals() {
        return rentService.getActiveRents();
    }

    @GetMapping("/{id}")
    public RentDTO getRentalById(@PathVariable(value = "id") long id){
        return rentService.getRentById(id);
    }

    @GetMapping("/users/{userId}")
    public List<RentDTO> getRentalsByUserId(@PathVariable(value = "userId") long userId){
        return rentService.getRentsByUserId(userId);
    }

    @GetMapping("/active/users/{userId}")
    public List<RentDTO> getActiveRentalsByUserId(@PathVariable(value = "userId") long userId){
        return rentService.getActiveRentsByUserId(userId);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ECARRENT_ADMIN')")
    public ResponseEntity<?> deleteRentalById(@PathVariable(name = "id") long id){
        rentService.deleteRentById(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{id}/active")
    @PreAuthorize("hasRole('ECARRENT_ADMIN')")
    public ResponseEntity<?> setRentalActive(@PathVariable(name = "id") long id){
        rentService.setRentActive(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{id}/inactive")
    @PreAuthorize("hasRole('ECARRENT_ADMIN')")
    public ResponseEntity<?> setRentalInactive(@PathVariable(name = "id") long id){
        rentService.setRentInactive(id);
        return ResponseEntity.ok().build();
    }
}
