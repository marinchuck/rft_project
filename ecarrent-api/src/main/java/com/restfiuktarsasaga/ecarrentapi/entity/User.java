package com.restfiuktarsasaga.ecarrentapi.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class User extends Auditable{

    @NonNull
    private String username;

    @NonNull
    private String email;

    @NonNull
    private String firstName;

    @NonNull
    private String lastName;

    private boolean enabled;

    @NonNull
    private String hashedPass;

    @OneToMany(cascade = CascadeType.ALL/*,mappedBy = "renter"*/)
    private List<Rent> rents;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<Authority> authorities;
}
