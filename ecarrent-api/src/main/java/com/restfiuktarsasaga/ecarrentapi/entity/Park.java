package com.restfiuktarsasaga.ecarrentapi.entity;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Park extends Auditable{

    @NonNull
    private String country;

    @NonNull
    private String city;

    @NonNull
    private String street;

    @NonNull
    private String streetNumber;

    @OneToMany(cascade = CascadeType.ALL/*, mappedBy = "park"*/)
    private List<Car> cars;
}
