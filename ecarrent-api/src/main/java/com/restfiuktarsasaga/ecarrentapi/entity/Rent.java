package com.restfiuktarsasaga.ecarrentapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Entity
@Data
@NoArgsConstructor
public class Rent extends Auditable {

    @NonNull
    private String token;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "car_id")
    @JsonIgnore
    private Car car;

    //@ManyToOne(fetch = FetchType.EAGER, optional = false)
    //@JoinColumn(name = "renter_id")
    //@JsonIgnore
    //private User renter;

    @NonNull
    private LocalDateTime rentStartedAt;

    @NonNull
    private LocalDateTime rentEndsAt;

    private boolean active;

    private int price;

    // TODO TEST IT
    //@Column(name = "price")
    //public int getPrice(){
    //    return car.getPrice() * (int)(rentStartedAt.until(rentEndsAt, ChronoUnit.DAYS));
    //}
}
