package com.restfiuktarsasaga.ecarrentapi.entity;

import com.restfiuktarsasaga.ecarrentapi.enums.Gearbox;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
public class Car extends Auditable{

    @NonNull
    private String make;

    @NonNull
    private String model;

    @NonNull
    private String type;

    private int power;

    private int seats;

    private int doors;

    @Enumerated(EnumType.STRING)
    @NonNull
    @Column(length = 10)
    private Gearbox gearbox;

    private boolean airConditioning;

    @NonNull
    private String color;

    @NonNull
    private String fuelType;

    @NonNull
    private LocalDateTime firstRegistration;

    //Egy napra vonatkozó ár
    private int price;

    private String registrationNumber;

    //@ManyToOne(fetch = FetchType.LAZY, optional = false)
    //@JoinColumn(name = "park_id")
    //@JsonIgnore
    //private Park park;

    //@OneToMany(cascade = CascadeType.ALL,mappedBy = "car")
    //private List<Rent> rents;
}
