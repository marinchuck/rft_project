package com.restfiuktarsasaga.ecarrentapi.dto.park;

import com.restfiuktarsasaga.ecarrentapi.dto.DtoBase;
import com.restfiuktarsasaga.ecarrentapi.dto.car.CarDTO;
import lombok.Data;

import java.util.List;

@Data
public class ParkDTO extends DtoBase {

    private String country;

    private String city;

    private String street;

    private String streetNumber;

    private List<CarDTO> cars;
}
