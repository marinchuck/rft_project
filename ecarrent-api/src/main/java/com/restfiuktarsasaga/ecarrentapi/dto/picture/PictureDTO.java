package com.restfiuktarsasaga.ecarrentapi.dto.picture;

import com.restfiuktarsasaga.ecarrentapi.dto.DtoBase;
import lombok.Data;

@Data
public class PictureDTO extends DtoBase {

    private String filename;

    private String fileType;

    private byte[] data;
}
