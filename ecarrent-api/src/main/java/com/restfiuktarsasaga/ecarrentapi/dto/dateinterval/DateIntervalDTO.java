package com.restfiuktarsasaga.ecarrentapi.dto.dateinterval;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class DateIntervalDTO {

    private LocalDateTime dateStart;

    private LocalDateTime dateEnd;
}
