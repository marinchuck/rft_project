package com.restfiuktarsasaga.ecarrentapi.dto.car;

import com.restfiuktarsasaga.ecarrentapi.enums.Gearbox;
import com.restfiuktarsasaga.ecarrentapi.validators.car.ValidRegNumber;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class CreateCarDTO {

    @NotNull
    private String make;

    @NotNull
    private String model;

    @NotNull
    private String type;

    @Min(60)
    @Max(800)
    private int power;

    @Min(2)
    @Max(9)
    private int seats;

    @Min(2)
    @Max(5)
    private int doors;

    @NotNull
    private Gearbox gearbox;

    @NotNull
    private boolean airConditioning;

    @NotNull
    private String color;

    @NotNull
    private String fuelType;

    @NotNull
    private LocalDateTime firstRegistration;

    @ValidRegNumber
    @NotNull
    private String registrationNumber;

    //Egy napra vonatkozó ár
    @NotNull
    @Min(2000)
    @Max(100000)
    private int price;
}

