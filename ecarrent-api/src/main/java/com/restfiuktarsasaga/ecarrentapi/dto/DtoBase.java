package com.restfiuktarsasaga.ecarrentapi.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class DtoBase {

    private Long id;

    private LocalDateTime createDate;

    private LocalDateTime updateDate;
}
