package com.restfiuktarsasaga.ecarrentapi.dto.user;

import com.restfiuktarsasaga.ecarrentapi.validators.user.UniqueEmail;
import com.restfiuktarsasaga.ecarrentapi.validators.user.UniqueUserName;
import com.restfiuktarsasaga.ecarrentapi.validators.user.ValidEmail;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CreateUserDTO {

    @UniqueUserName
    @NotBlank
    @NotNull
    private String username;

    @ValidEmail
    @UniqueEmail
    @NotBlank
    @NotNull
    private String email;

    @NotBlank
    @NotNull
    private String firstName;

    @NotBlank
    @NotNull
    private String lastName;

    @NotBlank
    @NotNull
    private String hashedPass;
}
