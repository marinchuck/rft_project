package com.restfiuktarsasaga.ecarrentapi.dto.car;

import com.restfiuktarsasaga.ecarrentapi.dto.DtoBase;
import com.restfiuktarsasaga.ecarrentapi.enums.Gearbox;
import com.restfiuktarsasaga.ecarrentapi.validators.car.ValidRegNumber;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class CarDTO extends DtoBase {

    private String make;

    private String model;

    private String type;

    private int power;

    private int seats;

    private int doors;

    private Gearbox gearbox;

    private boolean airConditioning;

    private String color;

    private String fuelType;

    private LocalDateTime firstRegistration;

    @ValidRegNumber
    private String registrationNumber;

    //Egy napra vonatkozó ár
    private int price;

    //private ParkDTO park;

    //private List<RentDTO> rents;
}
