package com.restfiuktarsasaga.ecarrentapi.dto.user;

import com.restfiuktarsasaga.ecarrentapi.dto.DtoBase;
import com.restfiuktarsasaga.ecarrentapi.dto.rent.RentDTO;
import lombok.Data;

import java.util.List;

@Data
public class UserDTO extends DtoBase {

        private String username;

        private String email;

        private String firstName;

        private String lastName;

        private boolean enabled;

        private List<RentDTO> rents;

}
