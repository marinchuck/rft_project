package com.restfiuktarsasaga.ecarrentapi.dto.park;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CreateParkDTO {

    @NotNull
    private String country;

    @NotNull
    private String city;

    @NotNull
    private String street;

    @NotNull
    private String streetNumber;

    //private List<CarDTO> cars;
}
