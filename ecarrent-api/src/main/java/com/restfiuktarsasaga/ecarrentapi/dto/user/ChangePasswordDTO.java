package com.restfiuktarsasaga.ecarrentapi.dto.user;

import lombok.Data;

@Data
public class ChangePasswordDTO {

    private Long id;

    private String oldPassword;

    private String newPassword;
}
