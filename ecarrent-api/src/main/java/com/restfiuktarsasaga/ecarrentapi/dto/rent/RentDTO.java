package com.restfiuktarsasaga.ecarrentapi.dto.rent;

import com.restfiuktarsasaga.ecarrentapi.dto.DtoBase;
import com.restfiuktarsasaga.ecarrentapi.dto.car.CarDTO;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class RentDTO extends DtoBase {

    private String token;

    private CarDTO car;

    //private UserDTO renter;

    private LocalDateTime rentStartedAt;

    private LocalDateTime rentEndsAt;

    private boolean active;

    private int price;
}
