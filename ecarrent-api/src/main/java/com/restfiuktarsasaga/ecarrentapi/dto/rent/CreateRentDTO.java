package com.restfiuktarsasaga.ecarrentapi.dto.rent;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class CreateRentDTO {

    //private String token;

    //private CarDTO car;

    //private UserDTO renter;

    private LocalDateTime rentStartedAt;

    private LocalDateTime rentEndsAt;

    //private boolean active;

    //private int price;
}
