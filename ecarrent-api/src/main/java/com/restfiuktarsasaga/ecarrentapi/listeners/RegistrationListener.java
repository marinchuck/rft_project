package com.restfiuktarsasaga.ecarrentapi.listeners;

import com.restfiuktarsasaga.ecarrentapi.entity.User;
import com.restfiuktarsasaga.ecarrentapi.events.OnRegistrationCompleteEvent;
import com.restfiuktarsasaga.ecarrentapi.service.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

    private final UserService userService;

    private final JavaMailSender javaMailSender;

    @Override
    public void onApplicationEvent(OnRegistrationCompleteEvent onRegistrationCompleteEvent) {
        this.confirmRegistration(onRegistrationCompleteEvent);
    }

    private void confirmRegistration(OnRegistrationCompleteEvent event) {
        User user = event.getUser();
        String token = UUID.randomUUID().toString();
        userService.createRegistrationToken(user, token);

        String recipientAddress = user.getEmail();
        String subject = "Regisztráció megerősítés - ecarrent";
        String confirmationUrl
                = event.getAppUrl() + "/users/registrationConfirm?token=" + token;
        String message = "Kattintson a következő linkre a fiókja aktiválásahóz.";

        MimeMessagePreparator mimeMessagePreparator = new MimeMessagePreparator() {
            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                mimeMessage.setRecipient(Message.RecipientType.TO,new InternetAddress(recipientAddress));
                mimeMessage.setSubject(subject);
                mimeMessage.setText(message + "<br/>"
                        + "<a href='http://localhost:8083" + confirmationUrl +  "'>Aktivációs link. Kattintson ide!</a>","UTF-8","html");
            }
        };
        javaMailSender.send(mimeMessagePreparator);
    }
}
