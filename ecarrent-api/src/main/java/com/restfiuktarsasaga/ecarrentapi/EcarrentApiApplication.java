package com.restfiuktarsasaga.ecarrentapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class EcarrentApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(EcarrentApiApplication.class, args);
    }
}
