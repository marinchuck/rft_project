package com.restfiuktarsasaga.ecarrentapi.validators.car;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.validation.ConstraintValidatorContext;

public class RegNumberValidatorTest {

    private RegNumberValidator regNumberValidator;

    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        regNumberValidator = new RegNumberValidator();
    }

    @Test
    public void validRegNumber_isValid() {

        String regNumber = "ABC-123";
        boolean result = regNumberValidator.isValid(regNumber, constraintValidatorContext);
        boolean expected = true;
        Assert.assertEquals(expected,result);

    }

    @Test
    public void invalidRegNumber_isValid() {

        String regNumber = "ABC-1234";
        boolean result = regNumberValidator.isValid(regNumber, constraintValidatorContext);
        boolean expected = false;
        Assert.assertEquals(expected,result);

        regNumber = "ABCD-123";
        result = regNumberValidator.isValid(regNumber, constraintValidatorContext);
        Assert.assertEquals(expected,result);

        regNumber = "AbC-123";
        result = regNumberValidator.isValid(regNumber, constraintValidatorContext);
        Assert.assertEquals(expected,result);

        regNumber = "ABC-1f3";
        result = regNumberValidator.isValid(regNumber, constraintValidatorContext);
        Assert.assertEquals(expected,result);

        regNumber = "";
        result = regNumberValidator.isValid(regNumber, constraintValidatorContext);
        Assert.assertEquals(expected,result);

        regNumber = "ABC123";
        result = regNumberValidator.isValid(regNumber, constraintValidatorContext);
        Assert.assertEquals(expected,result);
    }
}