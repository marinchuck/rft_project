package com.restfiuktarsasaga.ecarrentapi.validators.user;

import com.restfiuktarsasaga.ecarrentapi.dao.UserDAO;
import com.restfiuktarsasaga.ecarrentapi.entity.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.validation.ConstraintValidatorContext;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class UniqueUserNameValidatorTest {

    @Mock
    private UserDAO userDAO;

    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    private UniqueUserNameValidator uniqueUserNameValidator;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        uniqueUserNameValidator = new UniqueUserNameValidator(userDAO);
    }

    @Test
    public void notUniqueUserName_isValid() {
        String username = "pista";
        when(userDAO.findUserByUsername(anyString())).thenReturn(Optional.of(new User()));

        boolean expected = false;
        boolean result = uniqueUserNameValidator.isValid(username, constraintValidatorContext);
        Assert.assertEquals(expected,result);
    }

    @Test
    public void uniqueUserName_isValid() {
        String username = "pista";
        when(userDAO.findUserByUsername(anyString())).thenReturn(Optional.ofNullable(null));

        boolean expected = true;
        boolean result = uniqueUserNameValidator.isValid(username, constraintValidatorContext);
        Assert.assertEquals(expected,result);
    }
}