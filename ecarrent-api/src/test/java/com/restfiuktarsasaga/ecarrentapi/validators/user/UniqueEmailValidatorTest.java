package com.restfiuktarsasaga.ecarrentapi.validators.user;

import com.restfiuktarsasaga.ecarrentapi.dao.UserDAO;
import com.restfiuktarsasaga.ecarrentapi.entity.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.validation.ConstraintValidatorContext;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class UniqueEmailValidatorTest {

    @Mock
    private UserDAO userDAO;

    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    private UniqueEmailValidator uniqueEmailValidator;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        uniqueEmailValidator = new UniqueEmailValidator(userDAO);
    }

    @Test
    public void uniqueEmail_isValid() {
        String email = "example@email.com";
        when(userDAO.findUserByEmail(anyString())).thenReturn(Optional.ofNullable(null));

        boolean expected = true;
        boolean result = uniqueEmailValidator.isValid(email,constraintValidatorContext);
        Assert.assertEquals(expected,result);
    }

    @Test
    public void notUniqueEmail_isValid() {
        String email = "example@email.com";
        when(userDAO.findUserByEmail(anyString())).thenReturn(Optional.of(new User()));

        boolean expected = false;
        boolean result = uniqueEmailValidator.isValid(email,constraintValidatorContext);
        Assert.assertEquals(expected,result);
    }
}