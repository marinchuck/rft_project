package com.restfiuktarsasaga.ecarrentapi.validators.user;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.validation.ConstraintValidatorContext;

public class ValidEmailValidatorTest {

    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    private ValidEmailValidator validEmailValidator;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        validEmailValidator = new ValidEmailValidator();
    }

    @Test
    public void emailValid_isValid() {

        String email = "valid@gmail.hu";
        boolean expected = true;
        boolean result = validEmailValidator.isValid(email,constraintValidatorContext);
        Assert.assertEquals(expected,result);

        email = "valid.really@gmail.hu";
        result = validEmailValidator.isValid(email,constraintValidatorContext);
        Assert.assertEquals(expected,result);

        email = "valid@gmail.de.hu";
        result = validEmailValidator.isValid(email,constraintValidatorContext);
        Assert.assertEquals(expected,result);
    }

    @Test
    public void emailInvalid_isValid() {

        String email = "invalid@gmail.h";
        boolean expected = false;
        boolean result = validEmailValidator.isValid(email,constraintValidatorContext);
        Assert.assertEquals(expected,result);

        email = "invalid.reallygmail.hu";
        result = validEmailValidator.isValid(email,constraintValidatorContext);
        Assert.assertEquals(expected,result);

        email = "@gmail.hu";
        result = validEmailValidator.isValid(email,constraintValidatorContext);
        Assert.assertEquals(expected,result);

        email = "invalid@gmailhu";
        result = validEmailValidator.isValid(email,constraintValidatorContext);
        Assert.assertEquals(expected,result);

        email = "invalid@gmailhu.";
        result = validEmailValidator.isValid(email,constraintValidatorContext);
        Assert.assertEquals(expected,result);
    }
}