### Lokalis futtatas:

1. application.properties aktiv profiljat dev-re kell allitani
2. letre kell hozni a sajat gepeden egy mysql adatbazist (localhost:3306) message_schema semaval (username: root, pass: '')
3. mvn package
4. java -jar target/ecarrent-api.jar
5. http://localhost:8080-on elerheto az alkalmazas az adatbazis meg termeszetesen localhost:3306-on

    ###### info: <java.version>1.8</java.version>





### Kontenerben valo futtatas:

1. application.properties aktiv profiljat prod-ra kell allitani
2. mvn package -DskipTests (fontos a kapcsolo hasznalata!!!)

    ###### itt vegre kell hajtani a repo gyokermappajanak README-jeben 'Kontenerben valo futtatas/ Elso -es/vagy Minden futtataskor vegrehatando' cim alatt szereplo pontokat, majd folytatni lehet az alabb irt 3as ponttal

3. http://localhost:8082-on elerheto az alkalmazas
(
4. az adatbazist ketfelekeppen is eleritek:
 - belovitek az altalatok hasznalt adatbazis klienst a localhost:3307-es portra (username: root, pass: example)
 - http://localhost:8081 (szerver: db, username: root, pass: example)
) 
