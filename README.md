## This project is aimed to be an implementation of a car renter application.
#### Developers: Gergo Hajzer, David Roszpapa, Tamas Marincsak
___


Az alkalmazas lokalis(dev) es kontenerizalt kornyezetben(prod) is futtathato.


### Lokalis futtatas:

1. Hajtsatok vegre az almappakban talalhato README-kben irtakat.



### Kontenerben valo futtatas:

1. Hajtsatok vegre az almappakban talalhato README-kben irtakat.

    ##### Elso futtataskor vegrehajtando:
1. Telepiteni kell a dockert.
2. Telepiteni kell a docker-compose-t.

    ##### Minden futtataskor vegrehajtando:
1. A repo gyokermappajaban ki kell adni a docker-compose up parancsot.
(
Ha vmi problema lenne, akkor Ctrl-c -vel megszakithatjatok a folyamatot.
Ha tovabbi problemak merulnek fel, akkor futtathatjatok a ddocker.sh-t, ami leallit minden futo kontenert es kitorol minden kontenert es imaget, es kezdhetitek elorol a folyamatot.
)

