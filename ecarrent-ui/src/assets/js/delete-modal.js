$(document).ready(function () {

    $(".img-box").css({ "opacity": "1" });

    $('.closeCar').on('click', function () {

        $('#deleteCar').css({ "left": "-200%" });

        setTimeout(function () {
            $('#deleteCarModal').modal("hide");
        }, 900);
    });


    $('#deleteCarModal').on('hide.bs.modal', function () {

        $('#deleteCar').css({ "left": "-200%" });
    });


    $('.closeUser').on('click', function () {

        $('#deleteUser').css({ "left": "-200%" });

        setTimeout(function () {
            $('#deleteUserModal').modal("hide");
        }, 900);
    });


    $('#deleteUserModal').on('hide.bs.modal', function () {

        $('#deleteUser').css({ "left": "-200%" });
    });


    $('.closePark').on('click', function () {

        $('#deletePark').css({ "left": "-200%" });

        setTimeout(function () {
            $('#deleteParkModal').modal("hide");
        }, 900);
    });


    $('#deleteParkModal').on('hide.bs.modal', function () {

        $('#deletePark').css({ "left": "-200%" });
    });


    $('#deleteCarBtn').on('click', function () {


        $('#deleteCarModal').modal().fadeIn(0);
        $('#deleteCar').css({ "left": "0" });
    });


    $('#deleteUserBtn').on('click', function () {


        $('#deleteUserModal').modal().fadeIn(0);
        $('#deleteUser').css({ "left": "0" });
    });


    $('#deleteParkBtn').on('click', function () {

        $('#deleteParkModal').modal().fadeIn(0);
        $('#deletePark').css({ "left": "0" });
    });

});