var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1;
var yyyy = today.getFullYear();
if (dd < 10) {
    dd = '0' + dd
}
if (mm < 10) {
    mm = '0' + mm
}

today = yyyy + '-' + mm + '-' + dd;
document.getElementById("rent-start").setAttribute("min", today);


$("#rent-start").change(function () {
    var min = $(this).val();
    document.getElementById("rent-end").setAttribute("min", min);
});