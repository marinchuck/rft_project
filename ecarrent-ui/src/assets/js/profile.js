(function () {
    $("#datasBtn").on('click', function () {
        $("#account-details").show();
        $("#rentings").hide();        
    });

    $("#rentsBtn").on('click', function () {
        $("#account-details").hide();
        $("#rentings").show();
    });

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
  }());
