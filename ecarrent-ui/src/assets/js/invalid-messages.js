// Park messages
var parkSelectBox = document.getElementById("park");
parkSelectBox.oninvalid = function () { parkSelectBoxMessage(this) };
parkSelectBox.oninput = function () {this.setCustomValidity("")};

function parkSelectBoxMessage(element) {
    var txt = "";
    if (element.validity.valueMissing) {
        txt = "Kérem válasszon egy telephelyet!";
    }

    element.setCustomValidity(txt);
}