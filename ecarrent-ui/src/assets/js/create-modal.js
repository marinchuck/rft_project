$(document).ready(function () {

    $(".img-box").css({ "opacity": "1" });

    $('.closeCar').on('click', function () {

        $('#createCar').css({ "left": "-200%" });

        setTimeout(function () {
            $('#createCarModal').modal("hide");
        }, 900);
    });


    $('#createCarModal').on('hide.bs.modal', function () {

        $('#createCar').css({ "left": "-200%" });
    });


    $('.closePark').on('click', function () {

        $('#createPark').css({ "left": "-200%" });

        setTimeout(function () {
            $('#createParkModal').modal("hide");
        }, 900);
    });


    $('#createParkModal').on('hide.bs.modal', function () {

        $('#createPark').css({ "left": "-200%" });
    });


    $('#createCarBtn').on('click', function () {


        $('#createCarModal').modal().fadeIn(0);
        $('#createCar').css({ "left": "0" });
    });


    $('#createParkBtn').on('click', function () {

        $('#createParkModal').modal().fadeIn(0);
        $('#createPark').css({ "left": "0" });
    });

});