export interface Car {
  id: number,
  make: string,
  model: string,
  type: any,
  power: number,
  seats: number,
  doors: number,
  gearbox: string,
  airConditioning: boolean,
  color: string,
  fuelType: string,
  price: string
}
