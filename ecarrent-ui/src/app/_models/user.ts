﻿export interface User {
  username: string,
  email: string,
  firstName: string,
  lastName: string,
  hashedPass: string
}
