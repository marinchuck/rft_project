export interface Rental {
  "id": number
  "createDate": string
  "updateDate": string
  "token": string
  "car": {
    "id": number,
    "createDate": string,
    "updateDate": string,
    "make": string,
    "model": string,
    "type": string,
    "power": number,
    "seats": number,
    "doors": number,
    "gearbox": string,
    "airConditioning": boolean,
    "color": string,
    "fuelType": string,
    "firstRegistration": string,
    "registrationNumber": string,
    "price": number
  },
  "rentStartedAt": string,
  "rentEndsAt": string,
  "active": boolean,
  "price": number
}
