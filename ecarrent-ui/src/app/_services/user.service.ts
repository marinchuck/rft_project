﻿import { Injectable } from '@angular/core';
import {AuthenticationService, ResourceService} from './index';
import {User} from "../_models";



@Injectable({ providedIn: 'root' })
export class UserService {

  constructor(private authencticationService: AuthenticationService, private _resourceService: ResourceService){}

  getUserInfo(){
    return this.authencticationService.getUserInfo();
  }

  registerUser(user : User){
    return this._resourceService.postData('/users/register', user);
  }

  getAllUsers(){
    return this._resourceService.getResourceFromApi('/users');
  }

  deleteUser(userId){
return this._resourceService.deleteResourceFromApi('/users/'+userId);
  }

  changePassword(id, oldPass, newPass){
    return this._resourceService.postData("/users/changePassword",{
  "id": id,
  "oldPassword": oldPass,
  "newPassword": newPass
    })
  }



}
