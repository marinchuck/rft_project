import { Injectable } from '@angular/core';
import {ResourceService} from "./resource.service";
import {UserService} from "./user.service";

@Injectable({
  providedIn: 'root'
})
export class RentService {

  constructor(private _resourceService : ResourceService, private _userService : UserService) { }

acceptRental(id){
    return this._resourceService.putResourceToApi("/rentals/" + id +"/inactive", {});
}

  postRental(carId, dateStart, dateEnd){
    return this._resourceService.postData('/rentals/cars/'+ carId + '/users/' + this._userService.getUserInfo().id, {
      "rentStartedAt": dateStart,
      "rentEndsAt": dateEnd
    });
  }
  getActiveRentals(){
    return this._resourceService.getResourceFromApi('/rentals/active/');
  }

  getRentalsByUserId(id){
    return this._resourceService.getResourceFromApi('/rentals/users/' + id);
  }

  deleteRental(id){
    return this._resourceService.deleteResourceFromApi('/rentals/' + id);
  }

  startRental(id){
    return this._resourceService.putResourceToApi("/rentals/" + id + "/active", {});
  }
}
