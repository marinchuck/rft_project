import { Injectable } from '@angular/core';
import {ResourceService} from './resource.service';
import {map} from 'rxjs/operators';
import {Car} from '../_models/car';
import {UserService} from "./user.service";

@Injectable({
  providedIn: 'root'
})
export class CarService {

  constructor(private _resourceService:ResourceService) { }

  // getCarPicture(id){
  //   return this._resourceService.getResourceFromApiBlob('/pictures/' + id);
  // }

  getCarsByParkId(parkId){
    console.log("getCarsByParkId");
  }

  getActiveRentals(){
    return this._resourceService.getResourceFromApi('/rentals/active/');
  }

  getAllCars(){
    return this._resourceService.getResourceFromApi('/cars');
      // .pipe(
      //   map(res =>
      //     {
      //       return res.json().results.map(
      //         item => {
      //           return new Car(
      //             item.make, item.model, item.type, item.power, item.seats, item.doors, item.gearbox, item.airconditioning, item.color, item.fuelType, item.price
      //           )
      //         }
      //       )
      //     }
      //   )
      // )
      // .pipe(
      //   map(data =>console.log(data)));
  }

  getCar(id: number){
    return this._resourceService.getResourceFromApi("/cars/"+id);
  }

  getAvailableCars(parkSelected, dateFrom, dateTo){
    const dateStart=dateFrom+"T00:00:00";
    const dateEnd=dateTo+"T00:00:00";
    return this._resourceService.getResourceFromApiWithParams("/cars/parks/"+parkSelected, {dateStart, dateEnd});
  }

  registerCar(data, parkSelected){
      return this._resourceService.postData('/cars/parks/' + parkSelected, data);
  }

  deleteCar(registrationNumber: string){
    return this._resourceService.deleteResourceFromApi('/cars/' + registrationNumber);
  }

}
