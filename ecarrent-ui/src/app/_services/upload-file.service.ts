import { Injectable } from '@angular/core';
import {HttpClient, HttpRequest, HttpEvent} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ResourceService} from "./resource.service";

@Injectable({
  providedIn: 'root'
})
export class UploadFileService {

  constructor(private _resourceService : ResourceService) {}

  pushFileToStorage(file: File){
    console.log("meghivodott fileservice");
    return this._resourceService.pushFileToStorage(file);
  }


}
