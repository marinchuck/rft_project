import { Injectable } from '@angular/core';
import {ResourceService} from './resource.service';

@Injectable({
  providedIn: 'root'
})
export class ParkService {

  constructor(private _resourceService: ResourceService) { }

  getAllParks(){
    return this._resourceService.getResourceFromApi('/parks');
  }

  registerPark(park){
    return this._resourceService.postData('/parks',park);
  }

  deletePark(parkId){
    return this._resourceService.deleteResourceFromApi('/parks/'+parkId);
  }
}
