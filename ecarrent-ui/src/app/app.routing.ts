﻿import { Routes, RouterModule } from '@angular/router';


import { AuthGuard } from './_guards';

import {LoginComponent} from './components/login/login.component';
import {HomeComponent} from './components/home/home.component';
import {RentComponent} from './components/rent/rent.component';
import {MyprofileComponent} from './components/myprofile/myprofile.component';
import {RegistrationComponent} from './components/registration/registration.component';
import {CreationComponent} from "./components/creation/creation.component";
import {AdminGuard} from "./_guards/adminauth.guard";
import {Page404Component} from "./components/page404/page404.component";
import {DeletePageComponent} from "./components/delete-page/delete-page.component";

const appRoutes: Routes = [
    { path: '', component: HomeComponent
      //, canActivate: [AuthGuard]
    },
  { path: 'foglalas', component: RentComponent
    , canActivate: [AuthGuard]
  },
    { path: 'login', component: LoginComponent },
  {
    path: 'profilom', component: MyprofileComponent, canActivate: [AuthGuard]
  },
  {
    path: 'regisztracio', component: RegistrationComponent
  },
  {
    path: 'letrehozas', component: CreationComponent, canActivate: [AdminGuard]
  },
  {
    path: 'error', component: Page404Component
  },
  {
    path: 'torles', component: DeletePageComponent
  },
    // otherwise redirect to '/'
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);
