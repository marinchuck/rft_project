import { Component, OnInit, AfterViewInit } from '@angular/core';
import {MatDialog, MatDialogConfig} from "@angular/material";
import {ModalParkCreationComponent} from "../modal-park-creation/modal-park-creation.component";
import {ModalParkDeletionComponent} from "../modal-park-deletion/modal-park-deletion.component";
import {ModalCarDeletionComponent} from "../modal-car-deletion/modal-car-deletion.component";
import {ModalUserDeletionComponent} from "../modal-user-deletion/modal-user-deletion.component";
import * as $ from 'jquery';

@Component({
  selector: 'app-delete-page',
  templateUrl: './delete-page.component.html',
  styleUrls: ['./delete-page.component.css']
})
export class DeletePageComponent implements OnInit,AfterViewInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  ngAfterViewInit(){
    $(document).ready(function () {
      $(".img-box").css({ "opacity": 1 });
    });
  }

  openParkDialog(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {

    };


    const dialogRef = this.dialog.open(ModalParkDeletionComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => { if (data) { } }
    );
  }

  openCarDialog(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {

    };


    const dialogRef = this.dialog.open(ModalCarDeletionComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => { if (data) { } }
    );
  }


  openUserDialog(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {

    };


    const dialogRef = this.dialog.open(ModalUserDeletionComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => { if (data) { } }
    );
  }


}
