import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCarDeletionComponent } from './modal-car-deletion.component';

describe('ModalCarDeletionComponent', () => {
  let component: ModalCarDeletionComponent;
  let fixture: ComponentFixture<ModalCarDeletionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCarDeletionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCarDeletionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
