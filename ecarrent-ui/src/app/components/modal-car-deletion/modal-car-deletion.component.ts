import { Component, Inject, OnInit } from '@angular/core';
import { Park } from "../../_models";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { CarService, ParkService } from "../../_services";

@Component({
  selector: 'app-modal-car-deletion',
  templateUrl: './modal-car-deletion.component.html',
  styleUrls: ['./modal-car-deletion.component.css']
})
export class ModalCarDeletionComponent implements OnInit {

  private registrationNumber: string;
  private myErrors = new Array();

  constructor(private dialogRef: MatDialogRef<ModalCarDeletionComponent>,
    @Inject(MAT_DIALOG_DATA) data, private _carService: CarService) { }

  ngOnInit() {

  }

  save() {
    this.delete();
  }

  close() {
    this.dialogRef.close(false);
  }

  delete() {
    if (this.registrationNumber) {
      this._carService.deleteCar(this.registrationNumber).subscribe(
        data => {
          this.dialogRef.close(true);
        },
        error => {
          this.myErrors = Array.of(error.error);
        }
      );
    }
  }

}
