import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from "@angular/router";
import { UserService } from "../../_services/user.service";
import { Car } from "../../_models/car";
import { AbstractControl, FormControl, ValidatorFn, Validators } from "@angular/forms";
import { formatDate } from "@angular/common";
import { MatDialog, MatDialogConfig } from "@angular/material";
import { ModalComponent } from "../modal/modal.component";
import { ModalCarCreationComponent } from "../modal-car-creation/modal-car-creation.component";
import * as $ from 'jquery';
import {ModalParkCreationComponent} from "../modal-park-creation/modal-park-creation.component";

@Component({
  selector: 'app-creation',
  templateUrl: './creation.component.html',
  styleUrls: ['./creation.component.css']
})
export class CreationComponent implements AfterViewInit {

  constructor(public dialog: MatDialog) { }

  ngAfterViewInit() {
    $(document).ready(function () {
      $(".img-box").css({ "opacity": 1 });
    });
  }

  openCarDialog() {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {

    };


    const dialogRef = this.dialog.open(ModalCarCreationComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => { if (data) { } }
    );
  }

  openParkDialog() {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {

    };


    const dialogRef = this.dialog.open(ModalParkCreationComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => { if (data) { } }
    );
  }

}
