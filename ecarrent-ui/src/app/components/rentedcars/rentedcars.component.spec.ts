import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RentedcarsComponent } from './rentedcars.component';

describe('RentedcarsComponent', () => {
  let component: RentedcarsComponent;
  let fixture: ComponentFixture<RentedcarsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RentedcarsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RentedcarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
