import { Component, OnInit } from '@angular/core';
import {UserService} from "../../_services/user.service";
import {RentService} from "../../_services/rent.service";
import {Rental} from "../../_models";

@Component({
  selector: 'app-my-rents',
  templateUrl: './my-rents.component.html',
  styleUrls: ['./my-rents.component.css']
})
export class MyRentsComponent implements OnInit {

  constructor(private _userService : UserService, private _rentalService : RentService) { }

  myErrors;

  rentals: Rental[];

  ngOnInit() {

    this._rentalService.getRentalsByUserId(this._userService.getUserInfo().id).subscribe(
      data => {
        console.log(data);
        this.rentals = data as Rental[];
      }
    );
  }

  deleteRental(id){
    this._rentalService.deleteRental(id).subscribe(
      data =>{},
      error=>{ console.log(error); this.myErrors = error.error.errors; },
      () => {
        let newRentalLlist = new Array();
        this.rentals.forEach(f => newRentalLlist.push(f));
        newRentalLlist.splice(this.rentals.findIndex(f => f.id == id), 1);
        this.rentals = newRentalLlist;
      }
      );
  }

}
