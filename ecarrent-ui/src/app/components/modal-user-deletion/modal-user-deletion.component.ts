import {Component, Inject, OnInit} from '@angular/core';
import {Park, User} from "../../_models";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {ParkService} from "../../_services";
import {UserService} from "../../_services/user.service";

@Component({
  selector: 'app-modal-user-deletion',
  templateUrl: './modal-user-deletion.component.html',
  styleUrls: ['./modal-user-deletion.component.css']
})
export class ModalUserDeletionComponent implements OnInit {

  userSelected;
users;


  constructor(private dialogRef: MatDialogRef<ModalUserDeletionComponent>,
              @Inject(MAT_DIALOG_DATA) data, private _userService : UserService) { }

  ngOnInit() {
this._userService.getAllUsers().subscribe(data => {
  this.users = data as User[];
},
error =>{
  console.log(error);
}
);


  }

  save() {
    this.delete();
    this.dialogRef.close(true);
  }

  close() {
    this.dialogRef.close(false);
  }

  delete() {
    if(this.userSelected){
      this._userService.deleteUser(this.userSelected).subscribe();
    }
  }
}
