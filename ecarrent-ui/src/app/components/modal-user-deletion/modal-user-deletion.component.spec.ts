import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalUserDeletionComponent } from './modal-user-deletion.component';

describe('ModalUserDeletionComponent', () => {
  let component: ModalUserDeletionComponent;
  let fixture: ComponentFixture<ModalUserDeletionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalUserDeletionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalUserDeletionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
