import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Park} from '../../_models';
import {Car} from '../../_models/car';
import {AuthenticationService, ParkService} from '../../_services';
import {FilterComponent} from '../filter/filter.component';
import {RentService} from "../../_services/rent.service";
import {ModalComponent} from "../modal/modal.component";
import {MatDialog, MatDialogConfig} from "@angular/material";


@Component({
  selector: 'app-rent',
  templateUrl: './rent.component.html',
  styleUrls: ['./rent.component.css']
})
export class RentComponent implements OnInit {

  carList: Car[];

  private _parkSelected;

  dateFrom;

  dateTo;

  carId;

  private _parkList : Park[] = [];

  constructor(private _parkService: ParkService, private _rentService : RentService, public dialog: MatDialog, private _authenticationService : AuthenticationService) {}

  ngOnInit() {
    this._parkService.getAllParks().subscribe(data => {this._parkList = data as Park[]})
  }

  isUser(){
    return this._authenticationService.isUser();
  }

  isAuthenticated(){
    return this._authenticationService.isAuthenticated();
  }

  isAdmin(){
    return this._authenticationService.isAdmin();
  }

  carsEvent(cars: Car []){
    this.carList = cars;
  }

  startDateEvent(startDate){
    this.dateFrom = startDate;
  }

  endDateEvent(endDate){
    this.dateTo = endDate;
  }

  carIdEvent(carId){
    this.carId = carId;
    this.openDialog();

  }

  get parkSelected() {
    return this._parkSelected;
  }

  set parkSelected(value) {
    this._parkSelected = value;
  }
  get parkList(): Park[] {
    return this._parkList;
  }

  set parkList(value: Park[]) {
    this._parkList = value;
  }

  openDialog() {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      message: 'Biztos ki akarja kölcsönözni?'
    };


    const dialogRef = this.dialog.open(ModalComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {if (data) this.makeRent(); }
    );
  }

  makeRent(){
    this._rentService.postRental(this.carId, this.dateFrom + "T00:00:00", this.dateTo + "T00:00:00").subscribe(result => {
      },
      error => {
        console.log(error)
      },
      () => {
        let newCarlist = new Array();
        this.carList.forEach(f => newCarlist.push(f));
        newCarlist.splice(this.carList.findIndex(f => f.id == this.carId), 1);
        this.carList = newCarlist;
      });
  }



}



