import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Car} from "../../_models/car";
import {CarService} from "../../_services";
import {RentService} from "../../_services/rent.service";
import {Rental} from "../../_models";

@Component({
  selector: 'app-rent-filter',
  templateUrl: './rent-filter.component.html',
  styleUrls: ['./rent-filter.component.css']
})
export class RentFilterComponent implements OnInit {
  rentals : Rental[];

  constructor(private _carService : CarService, private _rentService : RentService) { }

  ngOnInit() {
    this._rentService.getActiveRentals().subscribe(
      data=> {
        this.rentals = data as Rental[];
      }
    );
  }
  submit() {
    // {{url}}/rentals/active/

    // this._carService.getAvailableCars(this.parkSelected, this.dateFrom, this.dateTo).subscribe(data => {
    //   this.cars.emit(data as Car[]);
    //   this.startDate.emit(this.dateFrom);
    //   this.endDate.emit(this.dateTo);
    // });
  }

  acceptRental(id){
    this._rentService.acceptRental(id).subscribe(
      data =>{},
      error=>{},
    () => {
      let newRentalLlist = new Array();
      this.rentals.forEach(f => newRentalLlist.push(f));
      newRentalLlist.splice(this.rentals.findIndex(f => f.id == id), 1);
      this.rentals = newRentalLlist;
    }
    );
  }

  startRental(id){
    this._rentService.startRental(id).subscribe(
      data => {},
      error=> {console.log(error)},
      () => {}
    )
  }

}
