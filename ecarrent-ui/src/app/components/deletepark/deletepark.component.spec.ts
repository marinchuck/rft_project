import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteparkComponent } from './deletepark.component';

describe('DeleteparkComponent', () => {
  let component: DeleteparkComponent;
  let fixture: ComponentFixture<DeleteparkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteparkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteparkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
