import { Component, OnInit } from '@angular/core';
import {User} from '../../_models';
import {AuthenticationService} from '../../_services/';
import { UserService} from '../../_services/user.service';

@Component({
  selector: 'app-who-am-i',
  templateUrl: './who-am-i.component.html',
  styleUrls: ['./who-am-i.component.css']
})
export class WhoAmIComponent implements OnInit {
  get user(): User {
    return this._user;
  }

  set user(value: User) {
    this._user = value;
  }
  private _user: User;
  constructor(private _userService: UserService) {
  }

  ngOnInit() {
    // this._user = this._userService.user;
  }

  // getUserId(){
  //   return this.user.id;
  // }
  //
  // getUserEmail(){
  //   return this.user.email;
  // }

}
