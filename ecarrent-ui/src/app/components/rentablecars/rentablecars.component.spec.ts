import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RentablecarsComponent } from './rentablecars.component';

describe('RentablecarsComponent', () => {
  let component: RentablecarsComponent;
  let fixture: ComponentFixture<RentablecarsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RentablecarsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RentablecarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
