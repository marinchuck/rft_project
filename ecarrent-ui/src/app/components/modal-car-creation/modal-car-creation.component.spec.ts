import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCarCreationComponent } from './modal-car-creation.component';

describe('ModalCarCreationComponent', () => {
  let component: ModalCarCreationComponent;
  let fixture: ComponentFixture<ModalCarCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCarCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCarCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
