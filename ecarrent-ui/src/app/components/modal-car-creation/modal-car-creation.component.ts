import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {FileUploader} from "ng2-file-upload";
import {HttpResponse} from "../../../../node_modules/@angular/common/http";
import {formatDate} from "@angular/common";
import {AbstractControl, FormControl, ValidatorFn, Validators} from "@angular/forms";
import {CarService, ParkService} from "../../_services";
import {UploadFileService} from "../../_services/upload-file.service";
import {Park} from "../../_models";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-modal-car-creation',
  templateUrl: './modal-car-creation.component.html',
  styleUrls: ['./modal-car-creation.component.css']
})
export class ModalCarCreationComponent implements OnInit {


  make;
  type;
  model;
  color;
  fuelType;
  gearbox;
  parks : Park[] = [];
  parkSelected;
  registrationNumber;

  private _uploader: FileUploader = new FileUploader({url : environment.apiUrl + "/pictures", itemAlias: 'photo'});
  myErrors = new Array();
  selectedFiles: FileList;
  currentFileUpload: File;

  constructor(
    private dialogRef: MatDialogRef<ModalCarCreationComponent>,
    @Inject(MAT_DIALOG_DATA) data, private _carService : CarService, private uploadService: UploadFileService, private _parkService : ParkService) {

  }

  ngOnInit() {
    this._uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this._uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('ImageUpload:uploaded:', item, status, response);
    };
    this._parkService.getAllParks().subscribe(data => {
      this.parks = data as Park[];
    });
  }

  save() {
    this.upload();
    this.dialogRef.close(true);
  }

  close() {
    this.dialogRef.close(false);
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  imageUpload(){
    this.currentFileUpload = this.selectedFiles.item(0);
    return this.uploadService.pushFileToStorage(this.currentFileUpload);
  }

  carUpload(){
    console.log(formatDate(new Date(), 'yyyy-MM-dd', 'en'));
    if(this.airconditioning && this.color && this.doors.valid && this.fuelType && this.make && this.model && this.power.valid && this.seats.valid && this.type && this.gearbox && this.price.valid && this.parkSelected && this.registrationNumber){
      let dateNow = formatDate(new Date(this.firstRegistration.value + '-01-01'),'yyyy-MM-dd','en');
      let car = {
        "make": this.make,
        "model": this.model,
        "type": this.type,
        "power": this.power.value,
        "seats": this.seats.value,
        "doors": this.doors.value,
        "gearbox": this.gearbox,
        "airConditioning": this.airconditioning,
        "color": this.color,
        "fuelType": this.fuelType,
        "firstRegistration": dateNow + "T00:00:00",
        "price": this.price.value,
        "registrationNumber": this.registrationNumber
      };


this._carService.registerCar(car, this.parkSelected).subscribe();
    }
  }

  upload() {
    this.imageUpload().subscribe(
      event => {
        if (event instanceof HttpResponse) {
          console.log('File is completely uploaded!');
        }
      },
      error => {
        this.myErrors = error.error.errors;
      },
      () => {
        this.selectedFiles = undefined;
        this.carUpload();
      }
    );
  }

  price=new FormControl('', [
    Validators.required,
    this.numberRangeValidator(2000,100000)
  ]);
  airconditioning : boolean;
  power =new FormControl('', [
    Validators.required,
    this.numberRangeValidator(60,800)
  ]);

  seats =new FormControl('', [
    Validators.required,
    this.numberRangeValidator(2,9)
  ]);

  doors =new FormControl('', [
    Validators.required,
    this.numberRangeValidator(2,5)
  ]);

  firstRegistration = new FormControl('',[
    Validators.required,
    this.numberRangeValidator(1980,new Date().getFullYear())
  ]);



  numberRangeValidator(min: number, max: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      if (control.value !== undefined && (isNaN(control.value) || control.value < min || control.value > max)) {
        return { 'numberRange': true };
      }
      return null;
    };
  }


  get uploader(): FileUploader {
    return this._uploader;
  }

  set uploader(value: FileUploader) {
    this._uploader = value;
  }


}
