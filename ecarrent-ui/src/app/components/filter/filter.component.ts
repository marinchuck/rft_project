import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { NgForm, NgControl } from '@angular/forms';
import {Park} from '../../_models';
import {CarService, ParkService} from '../../_services';
import * as _moment from 'moment';
import {FormControl} from '@angular/forms';
import {Car} from '../../_models/car';

const moment = _moment;

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
  @Output() cars : EventEmitter<any> = new EventEmitter<any>();
  @Output() startDate : EventEmitter<any> = new EventEmitter<any>();
  @Output() endDate : EventEmitter<any> = new EventEmitter<any>();


  private _dateTo: string;

  private _dateFrom: string;

  private _parkSelected;

  private _parkList: Park[] = [];

  constructor(private _parkService: ParkService, private _carService: CarService) {
    // this.dateFrom = new FormControl('');
    // this.dateTo = new FormControl('');
  }

  ngOnInit() {
    this._parkService.getAllParks().subscribe(data => {
      this._parkList = data as Park[];
    });
  }

  submit() {
    this._carService.getAvailableCars(this.parkSelected, this.dateFrom, this.dateTo).subscribe(data => {
      this.cars.emit(data as Car[]);
      this.startDate.emit(this.dateFrom);
      this.endDate.emit(this.dateTo);
    });
  }

  setDateFrom(date) {
    this.dateFrom = moment(date).format('YYYY-MM-DD');
  }

  setDateTo(date) {
    this._dateTo = moment(date).format('YYYY-MM-DD');
  }


  get dateTo(): string {
    return this._dateTo;
  }

  set dateTo(value: string) {
    this._dateTo = value;
  }

  get dateFrom(): string {
    return this._dateFrom;
  }

  set dateFrom(value: string) {
    this._dateFrom = value;
  }

  get parkSelected() {
    return this._parkSelected;
  }

  set parkSelected(value) {
    this._parkSelected = value;
  }

  get parkList(): Park[] {
    return this._parkList;
  }

  set parkList(value: Park[]) {
    this._parkList = value;
  }

}
