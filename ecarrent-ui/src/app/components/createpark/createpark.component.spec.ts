import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateparkComponent } from './createpark.component';

describe('CreateparkComponent', () => {
  let component: CreateparkComponent;
  let fixture: ComponentFixture<CreateparkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateparkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateparkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
