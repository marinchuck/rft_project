import {Component, Inject, OnInit} from '@angular/core';
import {Park} from "../../_models";
import {FileUploader} from "ng2-file-upload";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {CarService, ParkService} from "../../_services";
import {UploadFileService} from "../../_services/upload-file.service";
import {formatDate} from "@angular/common";
import {stringify} from "querystring";
import {HttpResponse} from "@angular/common/http";
import {AbstractControl, FormControl, ValidatorFn, Validators} from "@angular/forms";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-modal-park-creation',
  templateUrl: './modal-park-creation.component.html',
  styleUrls: ['./modal-park-creation.component.css']
})
export class ModalParkCreationComponent implements OnInit {

// {
//   "country": "Magyarország",
//   "city": "Debrecen",
//   "street": "Piac utca",
//   "streetNumber": "13/A"
// }

  city;
  street;
  streetNumber;
  country;

  constructor(
    private dialogRef: MatDialogRef<ModalParkCreationComponent>,
    @Inject(MAT_DIALOG_DATA) data,
    private _parkService : ParkService
  ) {

  }

  ngOnInit() {

  }
  //
  save() {
    this.upload();
    this.dialogRef.close(true);
  }

  close() {
    this.dialogRef.close(false);
  }
  //

  //
  //
  //
  //
  //
  upload() {
    let park = {
      "country" : this.country,
      "street" : this.street,
      "streetNumber" : this.streetNumber,
      "city" : this.city
    };

    this._parkService.registerPark(park).subscribe();
  }
  //

  // doors =new FormControl('', [
  //   Validators.required,
  //   this.numberRangeValidator(2,5)
  // ]);
  //
  //
  //
  //
  // numberRangeValidator(min: number, max: number): ValidatorFn {
  //   return (control: AbstractControl): { [key: string]: boolean } | null => {
  //     if (control.value !== undefined && (isNaN(control.value) || control.value < min || control.value > max)) {
  //       return { 'numberRange': true };
  //     }
  //     return null;
  //   };
  // }

}
