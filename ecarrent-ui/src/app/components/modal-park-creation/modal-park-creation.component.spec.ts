import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalParkCreationComponent } from './modal-park-creation.component';

describe('ModalParkCreationComponent', () => {
  let component: ModalParkCreationComponent;
  let fixture: ComponentFixture<ModalParkCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalParkCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalParkCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
