import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthenticationService, LoginService, LogoutService } from '../../_services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  confirmed = false;
  myErrors = new Array();
  returnUrl: string;
  confirmationSuccess;
  private _userName;
  private _password;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private loginService: LoginService,
    private logoutService: LogoutService) { }

  ngOnInit() {

    this.confirmationSuccess = this.route.snapshot.queryParams['success'] || '';

    // reset login status
    this.logoutService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  onSubmit() {

    if (this.userName && this.password) {
      this.loginService.login(this.userName, this.password)
        .pipe(first())
        .subscribe(
          data => {
            this.confirmed = true;
            this.loginService.loggedIn = true;
            this.router.navigate([this.returnUrl]);
          },
          error => {
            this.myErrors = Array.of(error.error);
          });
    }
  }

  get userName() {
    return this._userName;
  }

  set userName(value) {
    this._userName = value;
  }

  get password() {
    return this._password;
  }

  set password(value) {
    this._password = value;
  }

  getErrorMessages(){
    return this.myErrors;
  }
}
