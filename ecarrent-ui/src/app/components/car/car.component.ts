import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CarService, ResourceService} from "../../_services";
import {Car} from "../../_models/car";
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

  @Input() private car: Car;

  @Output() carId : EventEmitter<any> = new EventEmitter<any>();

  // get id(): number {
  //   return this._id;
  // }
  //
  // set id(value: number) {
  //   this._id = value;
  // }

  private _id : number;

  image;
  imageSrc;


  constructor(private _carService:CarService, private resourceService : ResourceService, public _DomSanitizationService: DomSanitizer){}

  ngOnInit(): void {
    // this._carService.getCar(1).subscribe(data => {this._car = data as Car})
    // this._carService.getCarPicture(1).subscribe(response => {
    //   console.log(response);
    //   // this.image = response['image'];
    //   // this.imageSrc = 'data:image/jpeg;base64,' + this.image;
    // });
    this.getImageFromService();
  }

  imageToShow;

  isImageLoading;
  imageIsLoaded;

  setImageisLoadedToTrue(alma){
    console.log(alma);
    this.imageToShow = alma;
    this.imageIsLoaded = true;
  }

  createImageFromBlob(image: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this.imageToShow = false;
      this.setImageisLoadedToTrue(reader.result);

    }, false);

    if (image) {
      // console.log(image);
      reader.readAsDataURL(image);
    }
    this.imageIsLoaded=true;
  }


  getImageFromService() {
    // this.isImageLoading = true;
    this.resourceService.getImage("http://localhost:8083/pictures/1").subscribe(data => {
      this.createImageFromBlob(data);
      this.imageIsLoaded = true;
      // console.log(data);
      // this.isImageLoading = false;
      // console.log(this.imageToShow);
    }, error => {
      this.isImageLoading = false;
      console.log(error);
    }, () =>
      {


      }

    );
  }

  rentCar(){
    this.carId.emit(this.car.id);
  }

}
