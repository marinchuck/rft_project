import { Component, OnInit } from '@angular/core';
import {UserService} from "../../_services/user.service";

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {

complete = false;
  oldPassword;
password;

  constructor(private _userService : UserService) { }

  ngOnInit() {
  }

  submit(){
    if(this.password && this.oldPassword){
      this._userService.changePassword(this._userService.getUserInfo().id, this.oldPassword, this.password).subscribe(
        data => {},
        error=>{console.log(error)},
        ()=>{
          this.complete = true;
        }
      );
    }
  }

}
