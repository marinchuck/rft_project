import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from "../../_services/user.service";


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  confirmed = false;

  myErrors = new Array();

  hide1 = true;

  hide2 = true;

  private _email;

  private _firstName;

  private _lastName;

  private _userName;

  private _password;

  constructor(public router: Router, private _userService : UserService) { }

  submit(){

    if(this.userName && this.email && this.firstName && this.lastName && this.password){
      this._userService.registerUser({
        "username": this.userName,
        "email": this.email,
        "firstName": this.firstName,
        "lastName": this.lastName,
        "hashedPass": this.password
      }).subscribe(result => {
        },
        error => {
          this.myErrors = error.error.errors;
        },
        () => {
          this.confirmed = true;
          setTimeout(() => {
              this.router.navigate([''])
            }
            , 3000);

        });

    }
  }

  errorArrayContainsValue(propertyValue){
    return this.myErrors.some(e => e.field == propertyValue);
  }

  getErrorMessage(type){
    return this.myErrors.find(error => error['field'] == type).defaultMessage;
  }

  ngOnInit() {
  }

  get email() {
    return this._email;
  }

  set email(value) {
    this._email = value;
  }

  get firstName() {
    return this._firstName;
  }

  set firstName(value) {
    this._firstName = value;
  }

  get lastName() {
    return this._lastName;
  }

  set lastName(value) {
    this._lastName = value;
  }

  get userName() {
    return this._userName;
  }

  set userName(value) {
    this._userName = value;
  }

  get password() {
    return this._password;
  }

  set password(value) {
    this._password = value;
  }



}
