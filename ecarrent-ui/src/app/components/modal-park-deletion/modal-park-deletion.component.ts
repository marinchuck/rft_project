import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {ParkService} from "../../_services";
import {Park} from "../../_models";

@Component({
  selector: 'app-modal-park-deletion',
  templateUrl: './modal-park-deletion.component.html',
  styleUrls: ['./modal-park-deletion.component.css']
})
export class ModalParkDeletionComponent implements OnInit {

parks: Park[];

parkSelected;

  constructor(private dialogRef: MatDialogRef<ModalParkDeletionComponent>,
              @Inject(MAT_DIALOG_DATA) data, private _parkService : ParkService) { }

  ngOnInit() {
    this._parkService.getAllParks().subscribe(data => {
      this.parks = data as Park[];
    });
  }

  save() {
    this.delete();
    this.dialogRef.close(true);
  }

  close() {
    this.dialogRef.close(false);
  }

  delete() {
if(this.parkSelected){
   this._parkService.deletePark(this.parkSelected).subscribe();
    }


  }

}
