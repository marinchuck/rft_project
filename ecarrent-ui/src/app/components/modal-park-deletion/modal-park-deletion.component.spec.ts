import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalParkDeletionComponent } from './modal-park-deletion.component';

describe('ModalParkDeletionComponent', () => {
  let component: ModalParkDeletionComponent;
  let fixture: ComponentFixture<ModalParkDeletionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalParkDeletionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalParkDeletionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
