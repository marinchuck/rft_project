import { Component, OnInit } from '@angular/core';
import {AuthenticationService, LogoutService} from '../../_services';
import {Router} from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(private _authenticationService: AuthenticationService, private logoutService: LogoutService, private router: Router) { }

  ngOnInit() {
  }

  isAuthenticated(){
    return this._authenticationService.isAuthenticated();
  }

  logout(){
    this.logoutService.logout();
    this.router.navigate(['/login']);
  }

  isAdmin(){
    return this._authenticationService.isAdmin();
  }

}
