import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormControl, ValidatorFn, Validators} from "@angular/forms";
import {formatDate} from "@angular/common";
import {CarService} from "../../_services";
import {FileUploader} from "ng2-file-upload";
import {UploadFileService} from "../../_services/upload-file.service";
import {HttpEventType, HttpResponse} from "@angular/common/http";


const URL = 'http://localhost:8083/pictures';

@Component({
  selector: 'app-createauto',
  templateUrl: './createauto.component.html',
  styleUrls: ['./createauto.component.css']
})
export class CreateautoComponent implements OnInit {
  myErrors = new Array();
  imageUploaded = false;

  selectedFiles: FileList;
  currentFileUpload: File;
  progress: { percentage: number } = { percentage: 0 };



  private _uploader: FileUploader = new FileUploader({url : URL, itemAlias: 'photo'});

  ngOnInit() {
    this._uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this._uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('ImageUpload:uploaded:', item, status, response);
    };

  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  imageUpload(){
    this.currentFileUpload = this.selectedFiles.item(0);
    this.uploadService.pushFileToStorage(this.currentFileUpload).subscribe(
      event => {
        if (event instanceof HttpResponse) {
          console.log('File is completely uploaded!');
        }
      },
      error => {
        this.myErrors = error.error.errors;
      },
      () => {
        this.imageUploaded = true;

      }
    );

    this.selectedFiles = undefined
  }

  carUpload(){
    console.log(formatDate(new Date(), 'yyyy/MM/dd', 'en'));
    if(this.airconditioning && this.color && this.doors && this.fuelType && this.make && this.model && this.power && this.seats && this.type && this.gearbox && this.price){

    }
  }

  upload() {
    this.imageUpload();
    if(this.imageUploaded){
      this.carUpload();
    }

  }

  make;
  type;
  model;
  color;
  fuelType;
  gearbox;
  price=new FormControl('', [
    Validators.required,
    this.numberRangeValidator(2000,100000)
  ]);
  airconditioning : boolean;
  power =new FormControl('', [
    Validators.required,
    this.numberRangeValidator(20,1500)
  ]);

  seats =new FormControl('', [
    Validators.required,
    this.numberRangeValidator(1,200)
  ]);

  doors =new FormControl('', [
    Validators.required,
    this.numberRangeValidator(1,10)
  ]);




  numberRangeValidator(min: number, max: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      if (control.value !== undefined && (isNaN(control.value) || control.value < min || control.value > max)) {
        return { 'numberRange': true };
      }
      return null;
    };
  }


  constructor(private _carService : CarService, private uploadService: UploadFileService) {}



  get uploader(): FileUploader {
    return this._uploader;
  }

  set uploader(value: FileUploader) {
    this._uploader = value;
  }


}

