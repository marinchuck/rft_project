import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CarService} from '../../_services';
import {Car} from '../../_models/car';


@Component({
  selector: 'app-car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CarListComponent implements OnInit {

  @Input()
 carList: Car[];

  @Output() carId : EventEmitter<any> = new EventEmitter<any>();

  constructor(private _carService:CarService){}

  ngOnInit(): void {
    // this._carService.getAllCars().subscribe(data => {this._carList = data as Car[]})
  }

  carIdEvent(carId){
    this.carId.emit(carId);
  }

}
