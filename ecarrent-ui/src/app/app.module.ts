import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavComponent } from './components/nav/nav.component';
import { LoginComponent } from './components/login/login.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { routing } from './app.routing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ErrorInterceptor, JwtInterceptor} from './_helpers';
import { WhoAmIComponent } from './components/who-am-i/who-am-i.component';
import { CarListComponent } from './components/car-list/car-list.component';
import { CarComponent } from './components/car/car.component';
import { HomeComponent } from './components/home/home.component';
import { RentablecarsComponent } from './components/rentablecars/rentablecars.component';
import { RentedcarsComponent } from './components/rentedcars/rentedcars.component';
import { MyprofileComponent } from './components/myprofile/myprofile.component';
import { MydetailsComponent } from './components/mydetails/mydetails.component';
import { MyordersComponent } from './components/myorders/myorders.component';
import { NewcarComponent } from './components/newcar/newcar.component';
import { NewparkComponent } from './components/newpark/newpark.component';
import { NewComponent } from './components/new/new.component';
import { DeleteComponent } from './components/delete/delete.component';
import { DeletecarComponent } from './components/deletecar/deletecar.component';
import { DeleteparkComponent } from './components/deletepark/deletepark.component';
import { DeleteuserComponent } from './components/deleteuser/deleteuser.component';
import { RentComponent } from './components/rent/rent.component';
import { MatCardModule } from '@angular/material/card';
import {
  MatButtonModule,
  MatDatepicker,
  MatDatepickerModule, MatDialogModule,
  MatFormFieldModule, MatIcon, MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatSelectModule
} from '@angular/material';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FilterComponent } from './components/filter/filter.component';
import { ChangepasswordComponent } from './components/changepassword/changepassword.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { CreationComponent } from './components/creation/creation.component';
import {QuestionableBooleanPipe} from "./_helpers";
import { ModalComponent } from './components/modal/modal.component';
import { Page404Component } from './components/page404/page404.component';
import { CreateautoComponent } from './components/createauto/createauto.component';
import { CreateparkComponent } from './components/createpark/createpark.component';
import {FileSelectDirective} from "ng2-file-upload";
import { ModalCarCreationComponent } from './components/modal-car-creation/modal-car-creation.component';
import { ModalParkCreationComponent } from './components/modal-park-creation/modal-park-creation.component';
import { DeletePageComponent } from './components/delete-page/delete-page.component';
import { ModalParkDeletionComponent } from './components/modal-park-deletion/modal-park-deletion.component';
import { ModalCarDeletionComponent } from './components/modal-car-deletion/modal-car-deletion.component';
import { ModalUserDeletionComponent } from './components/modal-user-deletion/modal-user-deletion.component';
import { RentFilterComponent } from './components/rent-filter/rent-filter.component';
import { MyRentsComponent } from './components/my-rents/my-rents.component';



@NgModule({
  declarations: [
    FileSelectDirective,
    AppComponent,
    NavComponent,
    HomeComponent,
    LoginComponent,
    WhoAmIComponent,
    CarListComponent,
    CarComponent,
    HomeComponent,
    RentablecarsComponent,
    RentedcarsComponent,
    MyprofileComponent,
    MydetailsComponent,
    MyordersComponent,
    NewcarComponent,
    NewparkComponent,
    NewComponent,
    DeleteComponent,
    DeletecarComponent,
    DeleteparkComponent,
    DeleteuserComponent,
    RentComponent,
    FilterComponent,
    ChangepasswordComponent,
    RegistrationComponent,
    CreationComponent,
    QuestionableBooleanPipe,
    ModalComponent,
    Page404Component,
    CreateautoComponent,
    CreateparkComponent,
    ModalCarCreationComponent,
    ModalParkCreationComponent,
    DeletePageComponent,
    ModalParkDeletionComponent,
    ModalCarDeletionComponent,
    ModalUserDeletionComponent,
    RentFilterComponent,
    MyRentsComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    BrowserAnimationsModule,
    // MatDatepicker,
    // MatNativeDateModule,
    MatIconModule,
    MatMomentDateModule,
    MatDatepickerModule,
    MatDialogModule,
    routing
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    { provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    },
   
  ],
  bootstrap: [AppComponent],
  entryComponents: [ModalComponent, ModalCarCreationComponent, ModalParkCreationComponent, ModalParkDeletionComponent, ModalCarDeletionComponent, ModalUserDeletionComponent]
})
export class AppModule { }
