package com.restfiuktarsasaga.ecarrentAuthorizationServer.config.repository;

import com.restfiuktarsasaga.ecarrentAuthorizationServer.config.domain.Credentials;

public interface CustomCredentialRepositoryI {
    Credentials findUserByUsername(String username);
}
