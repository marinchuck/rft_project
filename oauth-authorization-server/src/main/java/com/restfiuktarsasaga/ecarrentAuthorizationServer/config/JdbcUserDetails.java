package com.restfiuktarsasaga.ecarrentAuthorizationServer.config;


import com.restfiuktarsasaga.ecarrentAuthorizationServer.config.domain.Credentials;

import com.restfiuktarsasaga.ecarrentAuthorizationServer.config.repository.CredentialRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Created by ahmed on 21.5.18.
 */
public class JdbcUserDetails implements UserDetailsService{

    @Autowired
    private CredentialRepositoryImpl credentialRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Credentials credentials = credentialRepository.findUserByUsername(username);



        if(credentials==null){

            throw new UsernameNotFoundException("User"+username+"can not be found");
        }

        User user = new User(credentials.getUsername(),credentials.getPassword(),true,true,true,true,credentials.getAuthorities());

        return  user;


    }
}
