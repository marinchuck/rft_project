package com.restfiuktarsasaga.ecarrentAuthorizationServer.config;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import java.util.HashMap;
import java.util.Map;

import com.restfiuktarsasaga.ecarrentAuthorizationServer.config.domain.Credentials;
import com.restfiuktarsasaga.ecarrentAuthorizationServer.config.repository.CredentialRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;


/**
 * Ez felel azert, hogy a visszaadott tokenben tovabbi adatokat tudjunk tarolni
 */
public class CustomTokenEnhancer implements TokenEnhancer {

    @Autowired
    private CredentialRepositoryImpl credentialRepository;


    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        final Map<String, Object> additionalInfo = new HashMap<>();
        Credentials credential = credentialRepository.findUserByUsername(authentication.getName());
        additionalInfo.put("id", credential.getId());
        additionalInfo.put("email", credential.getEmail());
        additionalInfo.put("firstName", credential.getFirstName());
        additionalInfo.put("lastName", credential.getLastName());
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
        return accessToken;
    }
}
