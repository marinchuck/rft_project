package com.restfiuktarsasaga.zuulProxy;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StreamUtils;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.POST_TYPE;

//@Component
public class PostFilterHeadersResponse extends ZuulFilter {

    @Autowired
    HttpSessionCollector httpSessionCollector;

    private static final Logger logger = LoggerFactory.getLogger(PostFilterHeadersResponse.class);

    @Value("${zuul.routes.oauth-server.url}")
    private String authServerAddress;


    @Override
    public String filterType() {
        return POST_TYPE;
    }

    @Override
    public int filterOrder() {
//        return SEND_RESPONSE_FILTER_ORDER - 1;
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return RequestContext.getCurrentContext().getRouteHost().toString().equals(authServerAddress);
    }

    @Override
    public Object run() {

        RequestContext currentContext = RequestContext.getCurrentContext();

        HttpSession httpSession = currentContext.getRequest().getSession();

        try {
            String data = StreamUtils.copyToString(currentContext.getResponseDataStream(), Charset.forName("UTF-8"));
            JSONObject jsonObj = new JSONObject(data);
            Iterator<String> keys = jsonObj.keys();
            while(keys.hasNext()) {
                String key = keys.next();
//                if (jsonObj.get(key) instanceof JSONObject) {
//
//                }
                httpSession.setAttribute(key, jsonObj.get(key));
                System.out.println(key + " - "+jsonObj.get(key));

            }
//            Map<String, HttpSession> sessions = httpSessionCollector.sessions;
//            System.out.println(sessions.toString());
            Enumeration alma = httpSession.getAttributeNames();
        while(alma.hasMoreElements()){
            System.out.println(alma.nextElement());
        }

        } catch (IOException e) {
            e.printStackTrace();
        }


        HttpServletResponse response = currentContext.getResponse();
        String header = response.getHeader("WWW-Authenticate");
        logger.info("POST_WWW-Authenticate:{}", header);
        Collection<String> headerNames = response.getHeaderNames();
        headerNames.forEach((name) -> {
            logger.info("POST_HEADER_NAMES:{} HEADER:{}", name, response.getHeader(name));
        });
        Map<String, String> zuulRequestHeaders = currentContext.getZuulRequestHeaders();
        zuulRequestHeaders.forEach((k, v) -> {
            logger.info("POST_ZuulRequestHeaders K:{} V:{}", k, v);
        });
        try {
            // check whether getResponseBody() or getResponseDataStream() is null ?
            String s = StreamUtils.copyToString(currentContext.getResponseDataStream(), Charset.forName("UTF-8"));
            logger.info("RESPONS_BODY_AS_STRING:{}", s);
            currentContext.setResponseDataStream(new ByteArrayInputStream(s.getBytes(Charset.forName("UTF-8"))));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}