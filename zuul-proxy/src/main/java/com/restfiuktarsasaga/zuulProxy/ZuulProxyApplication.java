package com.restfiuktarsasaga.zuulProxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@SpringBootApplication
//@EnableOAuth2Client
public class ZuulProxyApplication {
	private static final Logger logger = LoggerFactory.getLogger(PostFilterHeadersResponse.class);


	public static void main(String[] args) {

		SpringApplication.run(ZuulProxyApplication.class, args);

	}
}
